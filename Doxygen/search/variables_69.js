var searchData=
[
  ['idx',['idx',['../classgtbanswer_1_1_t_b_answer_1_1_columns.html#ace892cf4c26ac5c7fc97a9865c82d98b',1,'gtbanswer.TBAnswer.Columns.idx()'],['../classgtbquestion_1_1_t_b_question_1_1_columns.html#a46fc75402abcb8d7aa0d6fbc18621c17',1,'gtbquestion.TBQuestion.Columns.idx()']]],
  ['initboard',['initBoard',['../page__puzzle_8js.html#a462e35adb1c046c644fbe310f9ef562e',1,'page_puzzle.js']]],
  ['initinput',['initInput',['../page__puzzle_8js.html#aaad6d9623e433b32a4de4ee0bb32e2c8',1,'page_puzzle.js']]],
  ['initnoteoncell',['initNoteOnCell',['../page__puzzle_8js.html#af9bb0688a428e67a9f092ddaef7b1ce5',1,'page_puzzle.js']]],
  ['initundoredo',['initUndoRedo',['../page__puzzle_8js.html#a2c0ccc89d1c03a43a554f12ba3241377',1,'page_puzzle.js']]],
  ['isanswer',['isAnswer',['../page__puzzle_8js.html#a8552329c4369009519293fd165fc1454',1,'page_puzzle.js']]],
  ['isemptynote',['isEmptyNote',['../page__puzzle_8js.html#a2301eb6758284b2b84368fd75019c28d',1,'page_puzzle.js']]],
  ['isemptyvalue',['isEmptyValue',['../page__puzzle_8js.html#a31fbb9112bbb3c62b2b7dcb616376c12',1,'page_puzzle.js']]],
  ['isfixedcell',['isFixedCell',['../page__puzzle_8js.html#a40e9afd1a8abd8fad5853e3d5071ad06',1,'page_puzzle.js']]],
  ['isfullanswer',['isFullAnswer',['../page__puzzle_8js.html#aa7815bb0395a3b28b466633387d33e4a',1,'page_puzzle.js']]],
  ['ismobile',['isMobile',['../page__puzzle_8js.html#a31e50ce85bf458a4d1759eff04619c59',1,'page_puzzle.js']]],
  ['isnoteon',['isNoteON',['../page__puzzle_8js.html#aefa78880daf5b3e76c6223380e62a3bd',1,'page_puzzle.js']]],
  ['isselectedcell',['isSelectedCell',['../page__puzzle_8js.html#a2e7a091c52322661e45cca76d1d8639a',1,'page_puzzle.js']]],
  ['isvalidvalue',['isValidValue',['../page__puzzle_8js.html#aa5fe16d167a5c919ddee6a3576f4fbfb',1,'page_puzzle.js']]]
];
