var searchData=
[
  ['close',['close',['../classcdbsql_1_1_d_bsql.html#a9d76b0347ad5628fcd0a55dcceeb62f7',1,'cdbsql.DBsql.close()'],['../classclogs_1_1_logs.html#ac9cbc3abaa97bd0ce9ed9152aa5899c0',1,'clogs.Logs.close()'],['../classgdbaccessor_1_1_d_b_accessor.html#ac4299d946b8aa65b83899f0b0eaf1889',1,'gdbaccessor.DBAccessor.close()']]],
  ['columns',['Columns',['../classgtbdifficulty_1_1_t_b_difficulty_1_1_columns.html',1,'gtbdifficulty::TBDifficulty']]],
  ['columns',['Columns',['../classgtbversion_1_1_t_b_version_1_1_columns.html',1,'gtbversion::TBVersion']]],
  ['columns',['Columns',['../classgtbquestion_1_1_t_b_question_1_1_columns.html',1,'gtbquestion::TBQuestion']]],
  ['columns',['Columns',['../classgtbanswer_1_1_t_b_answer_1_1_columns.html',1,'gtbanswer::TBAnswer']]],
  ['common',['Common',['../namespace_common.html',1,'']]],
  ['count',['count',['../classgquestion_1_1_question.html#a3168810690dc0d8a46ed45b2bbffd63d',1,'gquestion.Question.count()'],['../classcquery_1_1_query.html#a8ef83c84001abf3d20819c78def62b32',1,'cquery.Query.count()']]],
  ['create',['create',['../classcquery_1_1_query.html#aab32a2e83ee91b5e17f225133c9a3499',1,'cquery::Query']]]
];
