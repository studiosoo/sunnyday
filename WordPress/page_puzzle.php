<?php
/// @file       page_puzzle.php
/// @brief      Connects SUNNYDAY database and gets random row from the TP_QUESTION table.

header('Content-type: application/x-javascript');

$SRC = "page_puzzle.php";
$FUNC = "global";
   
include_once("logs.php");
include_once("db_control.php");
include_once("tb_answer.php");
include_once("tb_question.php");
    
$host = 'localhost';//'192.168.0.14:3306';//"114.205.29.150:13306";
$user = 'sunnyday';
$passwd = 'Tjslepdl';
$database = 'SUNNYDAY';
$conn = db_connect($host, $user, $passwd, $database);
logs("connect database", $SRC, $FUNC);

$query = 'SELECT * FROM TB_QUESTION ORDER BY RAND() LIMIT 1;';
$rows = db_select($conn, $query);
    
$tbq = null;
for ($i = 0; $i < count($rows) && $tbq == null; $i++) {
    logs($rows[$i][6], $SRC, $FUNC);
    $tbq = new TB_Question;
    $tbq->init($rows[$i]);
}    

if ($tbq != null) {
    print('answer_seed="'.$tbq->answer_seed.'";');
    print('difficulty_level="'.$tbq->difficulty_level.'";');
    print('symmetry_type="'.$tbq->symmetry.'";');
    print('version="'.$tbq->version.'";');
    print('reg_tm="'.$tbq->reg_tm.'";');
    print('puzzle_str="'.$tbq->question_str.'";');
} else {
    print('puzzle_str="";');
}
/*
$query = 'SELECT * FROM TB_ANSWER WHERE seed="'.$tbq->answer_seed.'";';
$rows = db_select($conn, $query);

$tba = null;
for ($i = 0; $i < count($rows) && $tba == null; $i++) {
    logs($rows[$i][3], $SRC, $FUNC);
    $tba = new TB_Answer;
    $tba->init($rows[$i]);
}
 
if ($tba != null) {
    print('get_answer_str="'.$tba->answer_str.'";');
} else {
    print('get_answer_str="";');
}
*/
db_close($conn);
logs("close database", $SRC, $FUNC);

?>
