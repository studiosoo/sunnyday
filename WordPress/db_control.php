<?php
/// @file       db_control.php
/// @brief      Functions for accessing a database.

$SRC = "db_control.php";

include_once("logs.php");

/**
* @brief        db_connect initializes PDO object by parameters and returns it.
* @param        $_host A ip address or domain string of the database.
* @param        $_user A account name string for database access.
* @param        $_passwd A password string for database access.
* @param        $_database A database name string.
* @return       PDO object instance.
**/
//'114.205.29.150:13306', 'sunnyday', 'Tjslepdl', 'SUNNYDAY'
function db_connect($_host, $_user, $_passwd, $_database)
{
    //$conn = mysql_connect($_host, $_user, $_passwd) or die('Fail to connect database.');
    //mysql_select_db($_database);
    try {
        $host = 'mysql:host='.$_host.';dbname='.$_database.';charset=utf8';
        $conn = new PDO($host, $_user, $_passwd);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    }catch(PDOExcrption $ex){
        die('Fail to connect database. '.$ex->getMessage());
    }

    return $conn;
}

/**
* @brief        db_select Executes a query and returns the result.
* @param        $_conn A PDO object.
* @param        $_query A sql query string.
* @return       list of result row.
**/
function db_select($_conn, $_query)
{
    global $SRC;

    $FUNC = "db_select";

    $rows = null;
    try {
        $stmt = $_conn->prepare($_query);
        $stmt->execute();
        $count = $stmt->rowCount();
        logs("Result: ".strval($count), $SRC, $FUNC);

        $rows = $stmt->fetchAll();
    }catch(PDOExcption $ex){
        logs("ERROR: ".$ex->getMessage(), $SRC, $FUNC);
    }

    return $rows;
}

/**
* @brief        db_close Closes database connection.
* @param        $_conn A PDO object.
**/
function db_close($_conn)
{
    //mysql_close($_conn);
    $_conn = null;
}
?>
