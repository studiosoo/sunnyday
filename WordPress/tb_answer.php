<?php
/// @file       tb_answer.php
/// @brief      Represents TB_Answer class.

/** 
* @class TB_Answer
* 
* @brief Represents TB_ANSWER table.
*/
class TB_Answer
{
    /// @brief idx A index of the TB_ANSWER. 
    public $idx;
    /// @brief seed A number for unique answer creating. 
    public $seed;
    /// @brief version A unique number of the generator. 
    public $version;
    /// @brief reg_tm A registration time of the answer. 
    public $reg_tm;
    /// @brief answer_str A correct answer string.
    public $answer_str;

    /**
    * @brief        db_connect initializes object by parameter.
    * @param        $_row A string that contains Columns of the TB_ANSWER table.
    **/
    public function init($_row) {
        $this->idx = $_row[0];
        $this->seed = $_row[1];
        $this->version = $_row[2];
        $this->reg_tm = $_row[3];
        $this->answer_str = $_row[4];
    }
}
?>
