<?php
/// @file       tb_question.php
/// @brief      Represents TB_Question class.

/** 
* @class TB_Question
* 
* @brief Represents TB_QUESTION table.
*/
class TB_Question
{
    /// @brief idx A index of the TB_ANSWER. 
    public $idx;
    /// @brief answer_seed A number for unique answer creating. 
    public $answer_seed;
    /// @brief difficulty_level A level number of the question.  	
    public $difficulty_level;
    /// @brief symmetry A type number of symmetry. 
    public $symmetry;
    /// @brief seed A number for unique question creating.
    public $seed;
    /// @brief version A unique number of the generator. 
    public $version;
    /// @brief reg_tm A registration time of the answer. 
    public $reg_tm;
    /// @brief question_str A question string, 0 means blank.
    public $question_str;

    /**
    * @brief        db_connect initializes object by parameter.
    * @param        $_row A string that contains Columns of the TB_QUESTION table.
    **/
    public function init($_row) {
        $this->idx = $_row[0];
        $this->answer_seed = $_row[1];
        $this->difficulty_level = $_row[2];
        $this->symmetry = $_row[3];
        $this->seed = $_row[4];
        $this->version = $_row[5];
        $this->reg_tm = $_row[6];
        $this->question_str = $_row[7];
    }
}
?>
