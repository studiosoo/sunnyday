/// @file page_puzzle.js
/// @brief Module for wordpress puzzle page of the SunnyDay project.

{
    //board content values
    const EMPTY_STR = '0';
    const EMPTY_VAR = '';
    const MEMO_VAR = '<';

    //element.id
    const ELEMENT_ID_RESULT = 'result';
    const ELEMENT_ID_PUZZLE = 'puzzle';

    //device attribute
    const DEVICE_MOBILE = 'mobile';
    const DEVICE_DESKTOP = 'desktop';

    //element.class
    const CLASS_BOARD = "board";
    const CLASS_BOARD_BOTTOM = " bottom";
    const CLASS_BOARD_LEFT = " left";
    const CLASS_BOARD_CONTENT = "board_content";
    const CLASS_BOARD_CONTENT_MOBILE = "board_mobilecontent";
    const CLASS_INPUT = "input";
    const CLASS_INPUT_CONTENT = "input_content";
    const CLASS_INPUT_CONTENT_MOBILE = "input_mobilecontent";
    const CLASS_NOTE = "note";
    const CLASS_NOTE_MOBILE = "mobilenote";
    const CLASS_NOTE_CONTENT = "note_content";
    const CLASS_NOTE_CONTENT_MOBILE = "note_mobilecontent";

    //style.colors
    const BOARD_BGCOLOR_NORMAL = 'white';
    const BOARD_BGCOLOR_SAME_AREA_FIXED_ONNOTE = 'darkseagreen';
    const BOARD_BGCOLOR_SAME_AREA_ONNOTE = 'aquamarine';
    const BOARD_BGCOLOR_SAME_AREA_FIXED = 'darkkhaki';
    const BOARD_BGCOLOR_SAME_AREA = 'yellow';
    const BOARD_BGCOLOR_SELECTED = 'gainsboro';
    const BOARD_BGCOLOR_UNSELECTED = BOARD_BGCOLOR_NORMAL;
    const BOARD_BGCOLOR_FIXED = 'darkgray';
    const BOARD_BGCOLOR_UNFIXED = BOARD_BGCOLOR_NORMAL;
    const INPUT_BGCOLOR_INPUTABLE = BOARD_BGCOLOR_NORMAL;
    const INPUT_BGCOLOR_UNINPUTABLE = 'darkgray';
    const BOARD_FONTCOLOR_VALID = 'black';
    const BOARD_FONTCOLOR_INVALID = 'crimson';
    const BOARD_FONTWEIGHT_SAME = 'bold';
    const BOARD_FONTWEIGHT_UNSAME = 'normal';

    //array for undo/redo action recording.
    var UNDO = new Array();
    var REDO = new Array();

    /// @function SunnyDay.run
    /// Initializes puzzle page.
    var run = function () {
        showResultElement(false);

        initBoard(puzzle_str, '');
        initInput();
        initUndoRedo();
    };

    /// @function SunnyDay.showResultElement
    /// Shows ELEMENT_ID_RESULT element at the puzzle page.
    /// @param _disp Weather result element should be display. true is display, false otherwise.    
    var showResultElement = function (_disp) {
        document.getElementById(ELEMENT_ID_RESULT).style.display = 
            _disp ? 'inline' : 'none';
    };

    /// @function SunnyDay.initBoard
    /// Initializes puzzle board table.
    /// @param _puzzle_str A string of 81 char, '0' is blank.
    var initBoard = function (_puzzle_str) {    
        var table = document.createElement('TABLE');    
        table.setAttribute('class', CLASS_BOARD);

        var tableBody = document.createElement('TBODY');    
        {
            for(var r = 0; r < 9; r++) {
                var tr = document.createElement('TR');
                tr.setAttribute('class', CLASS_BOARD);

                for (var c = 0; c < 9; c++) {
                    var value =  _puzzle_str.substring(r * 9 + c, r * 9 + c + 1);

                    var td = document.createElement('TD');
                    td.id = getBoardCellId(r, c);
                    td.setAttribute('row', r);
                    td.setAttribute('column', c);
                    td.setAttribute('block', getBoardBlockIdx(r, c));
                    td.setAttribute('selected', false);
                    td.setAttribute('fixed', (value != EMPTY_STR));
                    setBoardCellBG(td);
                    
                    var cls = CLASS_BOARD;
		            if (r == 2 || r == 5) cls = cls + CLASS_BOARD_BOTTOM;
                    if (c == 3 || c == 6) cls = cls + CLASS_BOARD_LEFT;
                    td.setAttribute('class', cls);
        
                    var onclickHandler = function(td) {
                        return function() {
                            onclickBoardCell(this.id);
                        }
                    }
                    td.onclick = onclickHandler(td);
                    var ondblclickHandler = function(td) {
                        return function() {
                            ondblclickBoardCell(this.id);
                        }
                    }
                    td.ondblclick = ondblclickHandler(td);
                
                    {
                        var div = document.createElement('DIV');
                        div.setAttribute('class', CLASS_BOARD);
                        {
                            var content = document.createElement('DIV');
                            content.id = getBoardCellContentId(r, c);
                            content.setAttribute('class', isMobile() ? CLASS_BOARD_CONTENT_MOBILE : CLASS_BOARD_CONTENT);
                            content.innerHTML = (value != EMPTY_STR) ? value : EMPTY_VAR;	    

                            div.appendChild(content);
                        }
                        td.appendChild(div);
                    }
                    tr.appendChild(td);
                }//end for
                tableBody.appendChild(tr);
            }//end for
        }
        table.appendChild(tableBody);
    
        var puzzle = document.getElementById(ELEMENT_ID_PUZZLE);    
        if (puzzle) puzzle.appendChild(table);
    };

    /// @function SunnyDay.initInput
    /// Initializes puzzle input table.
    var initInput = function () {
        var table = document.createElement('TABLE');
        table.setAttribute('class', CLASS_INPUT);

        var tableBody = document.createElement('TBODY');
        {
            var tr = document.createElement('TR');
            {
                for (var i = 0; i < 10; i++) {
                    var td = document.createElement('TD');
                    td.id = getInputCellId(i);
                    td.setAttribute('class', CLASS_INPUT);
                    td.setAttribute('value', i.toString());
                        
                    setCellBG(td, false);
                
                    var onclickHandler = function(td) {
                        return function() {
                            onclickInputCell(this.id);
                        }
                    }
                    td.onclick = onclickHandler(td);
                    
                    {
                        var div = document.createElement('DIV');
                        div.setAttribute('class', CLASS_INPUT);
                        {
                            var content = document.createElement('DIV');
                            content.id = getInputCellContentId(i);
                            content.setAttribute('class', isMobile() ? CLASS_INPUT_CONTENT_MOBILE : CLASS_INPUT_CONTENT);

                            if (i == 0) content.setAttribute('note', false);
                            content.innerHTML = (i == 0) ? 'm' : i.toString();
                                                    
                            div.appendChild(content);
                        }
                        td.appendChild(div);
                    }
                    tr.appendChild(td);
                }//end for
            }
            tableBody.appendChild(tr);
        }
        table.appendChild(tableBody);
    
        var puzzle = document.getElementById(ELEMENT_ID_PUZZLE);    
        if (puzzle) puzzle.appendChild(table);
    };

    /// @function SunnyDay.initUndoRedo
    /// Initializes puzzle undo/redo table.
    var initUndoRedo = function () {
        UNDO = [];
        REDO = [];

        var table = document.createElement('TABLE');
        table.setAttribute('class', CLASS_INPUT);

        var tableBody = document.createElement('TBODY');
        {
            var tr = document.createElement('TR');
            {
                for (var i = 0; i < 2; i++) {
                    var td = document.createElement('TD');
                    td.id = (i == 0) ? 'id_undo' : 'id_redo';
                    td.setAttribute('class', CLASS_INPUT);
                    setCellBG(td, false);
                    var onclickHandler = function(td) {
                        return function() {
                            onclickUndoRedoCell(this.id);
                        }
                    }
                    td.onclick = onclickHandler(td);

                    {
                        var div = document.createElement('DIV');
                        div.setAttribute('class', CLASS_INPUT);
                        {
                            var content = document.createElement('DIV');
                            content.id = (i == 0) ? 'id_undo_content' : 'id_redo_content';
                            content.setAttribute('class', isMobile() ? CLASS_INPUT_CONTENT_MOBILE : CLASS_INPUT_CONTENT);
                            content.innerHTML = (i == 0) ? '<' : '>';
                                                    
                            div.appendChild(content);
                        }
                        td.appendChild(div);
                    }
                    tr.appendChild(td);
                }
            }
            tableBody.appendChild(tr);
        }
        table.appendChild(tableBody);
    
        var puzzle = document.getElementById(ELEMENT_ID_PUZZLE);    
        if (puzzle) puzzle.appendChild(table);
    };

    /// @function SunnyDay.onclickBoardCell
    /// Shows effects.
    /// @param _cellId A board cell id where the event occured.
    var onclickBoardCell = function (_cellId) {
        var lastSelected = null;
        for(var r = 0; r < 9; r++) {
            for (var c = 0; c < 9; c++) {
                var td = document.getElementById(getBoardCellId(r, c));
                setBoardCellBG(td);
                if (isSelectedCell(td)){                    
                    td.setAttribute('selected', false);
                    lastSelected = td;
                }            
            }
        }
    
        var selected = document.getElementById(_cellId);
        selected.setAttribute('selected', selected != lastSelected);
        if (selected == lastSelected) {            
            setBoardSameValue(null);
            setInputBG(false);
            return;
        }        
        setBoardCellBGSelected(selected);
        
        var sr = selected.getAttribute('row').toString();
        var sc = selected.getAttribute('column').toString();
        setBoardSameValue(document.getElementById(getBoardCellContentId(sr, sc)));        
        setBoardSameLineBlock(selected);
    
        setInputBG(!isFixedCell(selected));
    };
    
    /// @function SunnyDay.ondblclickBoardCell
    /// Clear board cell content.
    /// @param _cellId A board cell id where the event occured.
    var ondblclickBoardCell = function (_cellId) {
        var td = document.getElementById(_cellId);
        if (isFixedCell(td)) return;

        var r = td.getAttribute('row').toString();
        var c = td.getAttribute('column').toString();
        var content = document.getElementById(getBoardCellContentId(r, c));
        if (content) content.innerHTML = EMPTY_VAR;
        setBoardSameValue(content);  
        setBoardInvalidValue();              
    };

    /// @function SunnyDay.onclickInputCell
    /// Clear board cell content.
    /// @param _cellId A board cell id where the event occured.
    var onclickInputCell = function (_cellId) {
        var selected = document.getElementById(getSelectedBoardCellId());

        if (_cellId == getInputCellId(0)) {
            var content = document.getElementById(getInputCellContentId(0));
            content.setAttribute('note', !isNoteON())
            content.innerHTML = isNoteON() ? 'M' : 'm';
        
            setBoardSameLineBlock(selected);
            return;
        }
    
        if (selected == null || isFixedCell(selected)) return;

        var value = document.getElementById(_cellId).getAttribute('value');       

        var r = selected.getAttribute('row').toString();
        var c = selected.getAttribute('column').toString();
        var b = selected.getAttribute('block').toString();
        var content = document.getElementById(getBoardCellContentId(r, c));

        if (isNoteON()) {
            var nr = parseInt((value - 1) / 3);
            var nc = (value - 1) % 3;

            var noteContent = document.getElementById(getNoteCellContentId(r, c, nr, nc));
            if (noteContent == null) {
                content.innerHTML = EMPTY_VAR;
                setBoardCellContentColor(content, true);                
                setBoardCellContentFontWeight(content, false);

                setBoardSameValue(content);
                initNoteOnCell(r, c);
                noteContent = document.getElementById(getNoteCellContentId(r, c, nr, nc));
            }
            noteContent.innerHTML = (noteContent.innerHTML == value) ? EMPTY_VAR : value;
            if (isEmptyNote(r, c)) content.innerHTML = EMPTY_VAR;
            pushUndo(r, c, true, value);
        } else {
            content.innerHTML = (content.innerHTML == value) ? EMPTY_VAR : value;
            setBoardSameValue(content);
            pushUndo(r, c, false, value);
        }

        setBoardInvalidValue();

        if (isFullAnswer() && isAnswer(getAnswerFromBoard())) showResultElement(true);
    };

    /// @function SunnyDay.initNoteOnCell
    /// Initializes puzzle note table on the board cell.
    /// @param _r row index of the board cell.
    /// @param _c column index of the board cell.
    var initNoteOnCell = function (_r, _c) {
        var content = document.getElementById(getBoardCellContentId(_r, _c));

        var table = document.createElement('TABLE');
        table.id = getNoteId(_r, _c);
        table.setAttribute('class', CLASS_NOTE);

        var tableBody = document.createElement('TBODY');
        {
            for (var r = 0; r < 3; r++) {
                var tr = document.createElement('TR');
                tr.setAttribute('class', CLASS_NOTE);

                for (var c = 0; c < 3; c++) {
                    var td = document.createElement('TD');
                    td.setAttribute('class', CLASS_NOTE);

                    var ondblclickHandler = function(selected) {
                        return function() {
                            ondblclickBoardCell(selected.id);
                        }
                    }
		    selected = document.getElementById(getBoardCellId(_r, _c))
                    td.ondblclick = ondblclickHandler(selected);

                    {
                        var div = document.createElement('DIV');
                        div.setAttribute('class', isMobile() ? CLASS_NOTE_MOBILE : CLASS_NOTE);
                        {
                            var contentn = document.createElement('DIV');
                            contentn.id = getNoteCellContentId(_r, _c, r, c);
                            contentn.setAttribute('class', isMobile() ? CLASS_NOTE_CONTENT_MOBILE : CLASS_NOTE_CONTENT);
                            
                            div.appendChild(contentn);
                        }
                        td.appendChild(div);            
                    }
                    tr.appendChild(td);                   
                }
                tableBody.appendChild(tr);
            }
        }
        table.appendChild(tableBody);
        
        content.appendChild(table);
    };

    /// @function SunnyDay.setBoardSameLineBlock
    /// Sets background color of the board table td.
    /// @param _selected td of the board table.
    var setBoardSameLineBlock = function (_selected) {
        if (_selected == null) return;

        var sr = _selected.getAttribute('row').toString();
        var sc = _selected.getAttribute('column').toString();
        var sb = _selected.getAttribute('block').toString();

        for(var r = 0; r < 9; r++) {
            for (var c = 0; c < 9; c++) {
                if (r == sr && c == sc) continue;
                var td = document.getElementById(getBoardCellId(r, c));
                var b = td.getAttribute('block').toString();
                if (r == sr || c ==sc || b == sb) {
                    setBoardCellBGSameLineBlock(td);
                }
            }
        }
    };

    /// @function SunnyDay.setBoardCellBGSameLineBlock
    /// Sets background color of the board table td.
    /// @param _td td of the board table.
    var setBoardCellBGSameLineBlock = function (_td) {
        _td.style.backgroundColor = isFixedCell(_td) ? 
            (isNoteON() ? BOARD_BGCOLOR_SAME_AREA_FIXED_ONNOTE : BOARD_BGCOLOR_SAME_AREA_FIXED) : 
            (isNoteON() ? BOARD_BGCOLOR_SAME_AREA_ONNOTE : BOARD_BGCOLOR_SAME_AREA);
    };

    /// @function SunnyDay.isSelectedCell
    /// Returns weather note is empty, or not.
    /// @param _r row index of the board cell.
    /// @param _c column index of the board cell.
    /// @return true if the note is empty, false otherwise.
    var isEmptyNote = function (_r, _c) {
        if (document.getElementById(getNoteId(_r, _c)) == null) return true;

        for (var r = 0; r < 3; r++) {
            for (var c = 0; c < 3; c++) {
                var content = document.getElementById(getNoteCellContentId(_r, _c, r, c));
                if (content != null && content.innerHTML != EMPTY_VAR) return false;
            }
        }
        return true;
    };

    /// @function SunnyDay.isFullAnswer
    /// Returns weather the board is full of answer, or not.
    /// @return true if the board is full, false otherwise.
    var isFullAnswer = function () {
        for(var r = 0; r < 9; r++) {
            for (var c = 0; c < 9; c++) {
                var content = document.getElementById(getBoardCellContentId(r, c));
                if (content == null || isEmptyValue(content.innerHTML)) return false;
            }
        }
        return true;
    };

    /// @function SunnyDay.getAnswerFromBoard
    /// Returns A string that is converted from board cell contents.
    /// @return A string.
    var getAnswerFromBoard = function () {
        var inputs = '';
        for(var r = 0; r < 9; r++) {
            for (var c = 0; c < 9; c++) {
                var content = document.getElementById(getBoardCellContentId(r, c));
                inputs = inputs.concat(content == null ? EMPTY_STR : 
                    content.innerHTML ? content.innerHTML : EMPTY_STR);
            }
        }
        return inputs;
    };
        
    /// @function SunnyDay.isAnswer
    /// Returns weather _input is answer, or not.
    /// @return true if it is answer, false otherwise.
    var isAnswer = function (_input) {
        for (var r = 0; r < 9; r++){
            var row = _input.substring(r * 9, r * 9 + 9);
            var sum = 0;
            for (var i = 0; i < 9; i++) {
                var sum = sum + parseInt(row.substring(i, i + 1));
            }
            if (sum != 45) return false;
        }
        for (var c = 0; c < 9; c++){
            var column = '';
            for (var r = 0; r < 9; r++){
                column = column.concat(_input.substring(c + r * 9, c + r * 9 + 1));
            }
            var sum = 0;
            for (var i = 0; i < 9; i++) {
                var sum = sum + parseInt(column.substring(i, i + 1));
            }
            if (sum != 45) return false;
        }
        for (var b = 0; b < 9; b++) {
            var br = parseInt(b / 3) * 3
            var bc = b % 3 * 3
            var block = '';
            for (var r = br; r < br + 3; r++){
                for (var c = bc; c < bc + 3; c++){
                    block = block.concat(_input.substring(r * 9 + c, r * 9 + c + 1));
                }
            }
            var sum = 0;
            for (var i = 0; i < 9; i++) {
                var sum = sum + parseInt(block.substring(i, i + 1));
            }
            if (sum != 45) return false;
        }

        return true;
    };

    /// @function SunnyDay.getSelectedBoardCellId
    /// Returns td element id.
    /// @return A string or null if no selection occured.
    var getSelectedBoardCellId = function () {
        for(var r = 0; r < 9; r++) {
            for (var c = 0; c < 9; c++) {
                if (isSelectedCell(document.getElementById(getBoardCellId(r, c)))) return getBoardCellId(r, c);
            }
        }    
        return null;
    };

    
    /// @function SunnyDay.getBoardCellId
    /// Returns td element id.
    /// @return A string.
    var getBoardCellId = function (_r, _c) {
        return 'id_cell_' + (parseInt(_r) * 9 + parseInt(_c)).toString();
    };

    /// @function SunnyDay.getBoardCellContentId
    /// Returns content element id.
    /// @return A string.
    var getBoardCellContentId = function (_r, _c) {
        return 'id_cell_content_' + (parseInt(_r) * 9 + parseInt(_c)).toString();
    };

    /// @function SunnyDay.getNoteId
    /// Returns table element id.
    /// @return A string.
    var getNoteId = function (_r, _c) {
        return 'id_note_' + (parseInt(_r) * 9 + parseInt(_c)).toString();
    };

    /// @function SunnyDay.getNoteCellContentId
    /// Returns content element id.
    /// @return A string.
    var getNoteCellContentId = function (_r, _c, _nr, _nc) {
        return 'id_note_' + (parseInt(_r) * 9 + parseInt(_c)).toString() + '_content_' + (parseInt(_nr) * 3 + parseInt(_nc)).toString();
    };

    /// @function SunnyDay.getInputCellId
    /// Returns td element id.
    /// @return A string.
    var getInputCellId = function (_i) {
        return 'id_input_' + parseInt(_i).toString();
    };

    /// @function SunnyDay.getInputCellContentId
    /// Returns content element id.
    /// @return A string.
    var getInputCellContentId = function (_i) {
        return 'id_input_content_' + parseInt(_i).toString();
    };

    /// @function SunnyDay.isNoteON
    /// Returns weather note mode is on or not.
    /// @return true if note mode is on, false otherwise.
    var isNoteON = function () {
        return (document.getElementById(getInputCellContentId(0)).getAttribute('note').toString() == 'true')
    };

    /// @function SunnyDay.isSelectedCell
    /// Returns weather _td is selected or not.
    /// @param _td A td element of the board table.
    /// @return true if _td is selected, false otherwise.
    var isSelectedCell = function (_td) {
        return (_td.getAttribute('selected').toString() == 'true');
    };

    /// @function SunnyDay.isFixedCell
    /// Returns weather _td is fixed or not.
    /// @param _td A td element of the board table.
    /// @return true if _td is fixed, false otherwise.
    var isFixedCell = function (_td) {
        return (_td.getAttribute('fixed').toString() == 'true');
    };

    /// @function SunnyDay.isEmptyValue
    /// Returns weather _value is empty(or somthing like it).
    /// @param _value A value on the board cell.
    /// @return true if _value is empty, false otherwise.
    var isEmptyValue = function (_value) {
        return (_value == null || _value == EMPTY_VAR || _value.substring(0, 1) == MEMO_VAR);
    };

    /// @function SunnyDay.setBoardCellBGSelected
    /// Sets background color of the board cell on selecting.
    /// @param _td A td element of the board table.
    var setBoardCellBGSelected = function (_td) {
        _td.style.backgroundColor = 
            isFixedCell(_td) ? BOARD_BGCOLOR_SELECTED : BOARD_BGCOLOR_UNSELECTED;
    };
    
    /// @function SunnyDay.setBoardCellBG
    /// Sets background color of the board cell.
    /// @param _td A td element of the board table.
    var setBoardCellBG = function (_td) {
        _td.style.backgroundColor = 
            isFixedCell(_td) ? BOARD_BGCOLOR_FIXED : BOARD_BGCOLOR_UNFIXED;
    };    

    /// @function SunnyDay.isValidValue
    /// Sets all board table content div color with the value validation of the cell.
    var setBoardInvalidValue = function () {
        for (var r = 0; r < 9; r++) {
            for (var c = 0; c < 9; c++) {
                setBoardCellContentColor(document.getElementById(getBoardCellContentId(r, c)), isValidValue(r, c));
            }    
        }
    };

    /// @function SunnyDay.isValidValue
    /// Sets color of the board table content div.
    /// @param _r row index of the cell.
    /// @param _c column indexx of the cell.
    /// @return true if value is valid or empty, false otherwise.
    var isValidValue = function (_r, _c) {
        var content = document.getElementById(getBoardCellContentId(_r, _c));
        if (isEmptyValue(content.innerHTML)) return true;
        
        for (var i = 0; i < 9; i++) {
            if (i != _c) {
                var comp = document.getElementById(getBoardCellContentId(_r, i));
                if (comp && comp.innerHTML == content.innerHTML) return false;
            }
            if (i != _r) {
                var comp = document.getElementById(getBoardCellContentId(i, _c));
                if (comp && comp.innerHTML == content.innerHTML) return false;
            }
        }

        var b = getBoardBlockIdx(_r, _c);
        var br = parseInt(b / 3) * 3;
        var bc = b % 3 * 3;
        for (var r = br; r < br + 3; r++) { 
            for (var c = bc; c < bc + 3; c++) {
                if (r == _r && c == _c) continue;
                var comp = document.getElementById(getBoardCellContentId(r, c));
                if (comp && comp.innerHTML == content.innerHTML) return false;
            }
        }

        return true;
    };

    /// @function SunnyDay.setBoardCellContentColor
    /// Sets color of the board table content div.
    /// @param _content div having class value 'content' of the input table.
    /// @param _valid Weather the value on the content is valid. true is valid, false otherwise.
    var setBoardCellContentColor = function (_content, _valid) {
        _content.style.color = 
            _valid ? BOARD_FONTCOLOR_VALID : BOARD_FONTCOLOR_INVALID;
    };

    /// @function SunnyDay.setBoardSameValue
    /// Sets font weight of the board table content div.
    /// @param _selected div having class value 'content' of the input table.
    var setBoardSameValue = function (_selected) {
        var value = (_selected == null) ? 'not matched all' : 
            isEmptyValue(_selected.innerHTML) ? 'not matched all' : _selected.innerHTML;

        for (var r = 0; r < 9; r++) {
            for (var c = 0; c < 9; c++) {
                var content = document.getElementById(getBoardCellContentId(r, c));
                setBoardCellContentFontWeight(content, 
                  !isEmptyValue(content.innerHTML) && (content.innerHTML == value));
            }    
        }
    };

    /// @function SunnyDay.setBoardCellContentFontWeight
    /// Sets font weight of the board table content div.
    /// @param _content div having class value 'content' of the input table.
    /// @param _same Weather having same value with selected cell. true is same, false otherwise.
    var setBoardCellContentFontWeight = function (_content, _same) {
        _content.style.fontWeight = 
            _same ? BOARD_FONTWEIGHT_SAME : BOARD_FONTWEIGHT_UNSAME;
    };

    /// @function SunnyDay.setInputBG
    /// Sets background color of the input table.
    /// @param _able Weather selected cell is inputable(not fixed). true is inputable, false otherwise.
    var setInputBG = function (_able) {
        for (var i = 0; i < 10; i++) {
            setCellBG(document.getElementById(getInputCellId(i)), _able);
        }
    };

    /// @function SunnyDay.setCellBG
    /// Sets background color of the input table td.
    /// @param _td td of the input table.
    /// @param _able Weather selected cell is inputable(not fixed). true is inputable, false otherwise.
    var setCellBG = function (_td, _able) {
        _td.style.backgroundColor = 
            _able ? INPUT_BGCOLOR_INPUTABLE : INPUT_BGCOLOR_UNINPUTABLE;
    };

    /// @function SunnyDay.getBoardBlockIdx
    /// Returns a block index.
    /// @param _r A row index.
    /// @param _c A column index.
    /// @return A block index of the cell(_r, _c).
    var getBoardBlockIdx = function (_r, _c) {
        return parseInt(3 * parseInt(_r / 3) + parseInt(_c / 3));
    };

    /// @function SunnyDay.isMobile
    /// Returns weather divice attribute value is for mobile.
    /// @return true if 'device' attribute value is "mobile", false otherwise.
    var isMobile = function () {
        var puzzle = document.getElementById(ELEMENT_ID_PUZZLE);
        return (puzzle.getAttribute('device') == DEVICE_MOBILE);
    };

    /// @class SunnyDay.Input
    /// Represents user input action.
    /// @param _r A row index.
    /// @param _c A column index.
    /// @param _isNote true if this is note input.
    /// @param _value A input value.
    function Input(_r, _c, _isNote, _value) {
        this.r = _r;
        this.c = _c;
        this.isNote = _isNote;
        this.value = _value;
    }

    /// @function SunnyDay.pushUndo
    /// Records user input.
    /// @param _r A row index.
    /// @param _c A column index.
    /// @param _isNote true if this is note input.
    /// @param _value A input value.
    var pushUndo = function (_r, _c, _isNote, _value) {
        UNDO.push(new Input(_r, _c, _isNote, _value));
        REDO = [];
        setCellBG(document.getElementById('id_undo'), (UNDO.length != 0));
        setCellBG(document.getElementById('id_redo'), (REDO.length != 0));
    }

    /// @function SunnyDay.onclickUndoRedoCell
    /// Applies undo or redo action.
    /// @param _cellId A cell id where the event occured.
    var onclickUndoRedoCell = function (_cellId) {
        (_cellId == 'id_undo') ? undo() : redo();
    }

    /// @function SunnyDay.undo
    /// Applies undo action.
    var undo = function () {
        if (UNDO.length == 0) return;

        var input = UNDO.pop();
        REDO.push(input);
        setCellBG(document.getElementById('id_undo'), (UNDO.length != 0));
	    setCellBG(document.getElementById('id_redo'), (REDO.length != 0));

        applyInput(input);                 
    }

    /// @function SunnyDay.redo
    /// Applies undo action.
    var redo = function () {
        if (REDO.length == 0) return;

        var input = REDO.pop();
        UNDO.push(input);
	    setCellBG(document.getElementById('id_undo'), (UNDO.length != 0));
        setCellBG(document.getElementById('id_redo'), (REDO.length != 0));

        applyInput(input);
    }

    /// @function SunnyDay.redo
    /// Applies input action.
    /// @param _input A input action.
    var applyInput = function(_input) {
        var r = _input.r;
        var c = _input.c;
        var value = _input.value;
        var content = document.getElementById(getBoardCellContentId(r, c));
        if (_input.isNote) {
            var nr = parseInt((value - 1) / 3);
            var nc = (value - 1) % 3;
            var noteContent = document.getElementById(getNoteCellContentId(r, c, nr, nc));
            if (noteContent == null) {
                content.innerHTML = EMPTY_VAR;
                setBoardCellContentColor(content, true);                
                setBoardCellContentFontWeight(content, false);

                setBoardSameValue(content);
                initNoteOnCell(r, c);
                noteContent = document.getElementById(getNoteCellContentId(r, c, nr, nc));
            }
            noteContent.innerHTML = (noteContent.innerHTML == value) ? EMPTY_VAR : value;
            if (isEmptyNote(r, c)) content.innerHTML = EMPTY_VAR;

        } else {            
            content.innerHTML = (content.innerHTML == value) ? EMPTY_VAR : value;
            setBoardSameValue(content);
        }   
    }
}


//run
run();
