<?php
/// @file       logs.php
/// @brief      Writes log.

/**
* @brief        logs Writes log string.
* @param        $_log A log string.
* @param        $_src A source file name.
* @param        $_func A function name.
**/
function logs($_log, $_src, $_func)
{
    //echo($_log);
    error_log ("[".$_src."::".$_func."] ".$_log."\n", 3, "/usr/share/wordpress/wp-content/debug.log");
}
?>
