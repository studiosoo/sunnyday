## @package Common
#  ccommon.py
#
#  Global functions
import platform

## pathByPlatform
#  Makes path string for platform and returns.
def pathByPlatform(_path):
    if platform.system() == 'Windows':
        _path = _path.replace('/', '\\')
    #ei
    
    return _path
#ed

## grid2str
#  Makes grid[][] into str[] and returns.
def grid2str(_grid):
    rtn = ''
    for r in range(9):
        for c in range(9):
            rtn = rtn + str(_grid[r][c])
        #ef
    #ef
    
    return rtn
#ed