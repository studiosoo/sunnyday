## @package Common
#  clogs.py
#
#  Represents Logs class
import os
import shutil
import platform
import sys
from time import localtime, strftime

from ccommon import *

## gLogs
#
#  Global variable for singleton pattern.
gLogs = None

## initLogs
#
#  Initialize gLog object.
#  @param _name A module name to be written in log.
#  @patam _rootPath A path logPath to be created.
def logInit(_name, _rootPath):
    global gLogs
    
    gLogs = Logs(_name, _rootPath)
#ed

## logLevel
#
#  Sets log level
#  @param _level A maximum log type to be written.
def logLevel(_level):
    global gLogs 
    
    gLogs.logLevel = _level
#ed

## logCRI
#
#  Writes critial type log.
#  @param _log A log string.
def logCRI(_log):
    global gLogs 
    
    gLogs.writeCRITICAL(_log)
#ed

## logERR
#
#  Writes error type log.
#  @param _log A log string.
def logERR(_log):
    global gLogs 
    
    gLogs.writeERROR(_log)
#ed

## logDBG
#
#  Writes debug type log.
#  @param _log A log string.
def logDBG(_log):
    global gLogs 
    
    gLogs.writeDEBUG(_log)
#ed

## logINF
#
#  Writes information type log.
#  @param _log A log string.
def logINF(_log):
    global gLogs 
    
    gLogs.writeINFO(_log)
#ed

## Logs
#
#  Writes log strings into .log file and manages in time.
class Logs:
    ## @var __name
    #  A module name to be written in log.
    ## @var __rootPath    
    #  A path logPath to be created.
    ## @var __logPath
    #  A path logFile to be Created.
    ## @var __filePath    
    #  A .log file path.
    ## @var __nowHour    
    #  The hour at which the log is written.
    ## @var __nowDay    
    #  The day at which the log is written.
    ## @var __logFile    
    #  The log file path.
    ## @var __logLevel 
    #  A maximum log type to be written, the default value is TP_INFO.
    ## @var __pathMark
    #  A path separation string for the current platform.
        
    ## TP_CRITICAL
    #  The log type Critical means it can not go any further.
    TP_CRITICAL = 0
    
    ## TP_ERROR
    #  The log type Error means it was not done as intended.
    TP_ERROR = 1
    
    ## TP_DEBUG
    #  The log type Debug means the value you want to check.
    TP_DEBUG = 2
    
    ## TP_INFO
    #  The log type Information means the progress of the program.
    TP_INFO = 3
    
    ## TP_STRING
    #  The list of the short type name string.
    TP_STRING = ['CRI', 'ERR', 'DBG', 'INF']

    ## __init__
    #  Initializes internal state of the Logs object by _name and _rootPath.
    #  @param _name A module name to be written in log.
    #  @patam _rootPath A path logPath to be created.
    def __init__(self, _name, _rootPath):        
        self.__name  = _name
        self.__rootPath = _rootPath
        self.__nowHour = strftime("%H", localtime())
        self.__nowDay = strftime("%m%d", localtime())
        self.__logLevel = Logs.TP_INFO
        self.__pathMark = '\\'
        if platform.system() == 'Linux':
            self.__pathMark = '/'
        #ei        

        #check path else create
        if not os.path.exists(self.__rootPath):
            os.makedirs(self.__rootPath)
        #ef
        
        #check name else create
        self.__logPath = self.__rootPath + self.__pathMark + self.__name
        if not os.path.exists(self.__logPath):
            os.makedirs(self.__logPath)
        #ef
        
        self.__filePath =  self.__logPath + self.__pathMark + self.__name + '.log'
        self.__logFile = open(self.__filePath, 'ab')
    #ed
    
    ## __shiftHour
    #  Moves log file to date folder if time changes. And open new log file.
    def __shiftHour(self):
        hour = strftime("%H", localtime())
        if self.__nowHour == hour:
            return
        #ef
        
        path = self.__logPath + self.__pathMark + self.__nowDay
        if not os.path.exists(path):
            os.makedirs(path)
        #ef
        
        self.__logFile.close()
        path = self.__logPath  + self.__pathMark + self.__nowDay + self.__pathMark + self.__name + '.' + self.__nowHour  + '.log'
        shutil.move(self.__filePath, path)
        
        self.__nowHour = hour
        self.__nowDay = strftime("%m%d", localtime())
        self.__logFile = open(self.__filePath, 'ab')
    #ed

    ## __addPrefix
    #  Returns a log string with prefix.
    #  @param _logType A kind of logs.
    #  @parma _logCase An arbitrary string set by the user depending on the function.
    #  @param _logLine A log string.
    def __addPrefix(self, _logType, _logCase, _logLine):
        prefix = '[' + self.__name + ']'
        prefix += '[' + strftime("%m/%d %H:%M:%S", localtime()) + ']'
        if not _logCase == None:
            prefix += '[' + Logs.TP_STRING[_logType] + ':' + _logCase + ']'
        else:
            prefix += '[' + Logs.TP_STRING[_logType] + ']'
        #ei
        prefix += ' '
                                    
        return prefix + _logLine + '\n'
    #ed
    
    ## __writeLine
    #  Writes a log line with prefix.
    #  @param _logType A kind of logs.
    #  @parma _logCase An arbitrary string set by the user depending on the function.
    #  @param _logLine A log string.
    def __writeLine(self, _logType, _logCase, _logLine):
        if _logType < self.__logLevel:
            return
        #ei
        
        self.__shiftHour()
        line = self.__addPrefix(_logType, _logCase, _logLine)
        self.__logFile.write(line)
        
        #case
        print line.replace('\n', '')
    #ed

    ## __writeLine
    #  Writes log lines with prefix.
    #  @param _logType A kind of logs.
    #  @parma _logCase An arbitrary string set by the user depending on the function.
    #  @param _logLines A list of log string.
    def __writeLines(self, _logType, _logCase, _logLines):
        for line in _logLines:
            self.__writeLine(_logType, _logCase, line)
        #ef
    #ed
    
    ## writeCRITICAL
    #  Writes critial type log.
    #  @parma _logCase An arbitrary string set by the user depending on the function.
    #  @param _logLines A list of log string.
    def writeCRITICAL(self, _logLine, _logCase = None):
        self.__writeLine(Logs.TP_CRITICAL, _logCase, _logLine)
    #ed
    
    ## writeERROR
    #  Writes error type log.
    #  @parma _logCase An arbitrary string set by the user depending on the function.
    #  @param _logLines A list of log string.
    def writeERROR(self, _logLine, _logCase = None):
        self.__writeLine(Logs.TP_ERROR, _logCase, _logLine)
    #ed
    
    ## writeDEBUG
    #  Writes debug type log.
    #  @parma _logCase An arbitrary string set by the user depending on the function.
    #  @param _logLines A list of log string.
    def writeDEBUG(self, _logLine, _logCase = None):
        self.__writeLine(Logs.TP_DEBUG, _logCase, _logLine)
    #ed
    
    ## writeINFO
    #  Writes information type log.
    #  @parma _logCase An arbitrary string set by the user depending on the function.
    #  @param _logLines A list of log string.
    def writeINFO(self, _logLine, _logCase = None):
        self.__writeLine(Logs.TP_INFO, _logCase, _logLine)
    #ed

    ## close
    #  Closes the log file.
    def close(self):
        self.__logFile.close()
    #ed 
#ec
