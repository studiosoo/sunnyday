## @package Common
#  cproperty.py
#
#  Represents Property class
import xml.etree.ElementTree as et

from ccommon import *

## Property
#
#  Represents variables for this module which is loaded from property xml file.
class Property:
    ## @var module
    #  A Property.Module class instance.
    ## @var log
    #  A Property.Log class instance.
    ## @var database
    #  A Property.Database class instance.
    ## @var generator
    #  A Property.Generator class instance.

    ## Module
    #  The contents of the 'module' tag.
    class Module:
        ## @var __info{}
        #  A dictionary for contents.
        
        ## __init__
        #  Initializes internal state of the Module object.
        #  @param _name A module name.
        def __init__(self, _name):
            self.__info = {}
            self.__info['name'] = _name
        #ed

        ## get
        #  Returns value for _key.
        #  @param _key A key string.
        def get(self, _key):
            return self.__info[_key]
        #ed
        
        ## str
        #  Returns the string of all key-value contents.
        def str(self):
            return 'name[' + self.get('name') + ']'
        #ed
    #ec
    
    ## Module
    #  The contents of the 'log' tag.
    class Log:
        ## @var __info{}
        #  A dictionary for contents.
        
        ## __init__
        #  Initializes internal state of the Module object.
        #  @patam _path A string to be logs written.
        #  @param _level A maximum log type to be written.
        def __init__(self, _path, _level):
            self.info = {}
            self.info['path'] = _path
            self.info['level'] = _level
        #ed
        
        ## get
        #  Returns value for _key.
        #  @param _key A key string.
        def get(self, _key):
            return self.info[_key]
        #ed
        
        ## str
        #  Returns the string of all key-value contents.
        def str(self):
            return 'path[' + self.get('path') + '] level[' + str(self.get('level')) + ']'
        #ed
    #ec
    
    ## Module
    #  The contents of the 'database' tag.
    class Database:
        ## @var __info{}
        #  A dictionary for contents.
        
        ## __init__
        #  Initializes internal state of the Module object.
        #  @param _host A ip address of the database.
        #  @param _port A port number of the database.
        #  @param _name A database name string.
        #  @param _id A account name string for database access.
        #  @param _pw A password string for database access.
        def __init__(self, _host, _port, _name, _id, _pw):
            self.info = {}
            self.info['host'] = _host
            self.info['port'] = _port
            self.info['name'] = _name
            self.info['id'] = _id
            self.info['pw'] = _pw
        #ed
        
        ## get
        #  Returns value for _key.
        #  @param _key A key string.
        def get(self, _key):
            return self.info[_key]
        #ed
        
        ## str
        #  Returns the string of all key-value contents.
        def str(self):
            return 'host[' + self.get('host') + '] port[' + str(self.get('port')) + '] name[' + self.get('name') + '] id[' + self.get('id') + '] pw[' + self.get('pw') + ']' 
        #ed
    #ec
    
    ## Module
    #  The contents of the 'generator' tag.
    class Generator:
        ## @var __info{}
        #  A dictionary for contents.
        
        ## __init__
        #  Initializes internal state of the Module object.
        #  @param _initialAnswerSeed the first hash value for random.
        #  @param _answerCount A number of answers to create.
        #  @param _questionCount A number of questions per level, symmetry in one answer. 
        def __init__(self, _initialAnswerSeed, _answerCount, _questionCount):
            self.info = {}
            self.info['initial_answer_seed'] = _initialAnswerSeed
            self.info['answer_count'] = _answerCount
            self.info['question_count'] = _questionCount
        #ed
        
        ## get
        #  Returns value for _key.
        #  @param _key A key string.
        def get(self, _key):
            return self.info[_key]
        #ed
        
        ## str
        #  Returns the string of all key-value contents.
        def str(self):
            return 'answer_count[' + str(self.get('answer_count')) + '] question_count[' + str(self.get('question_count')) + ']'
        #ed
    #ec
    
    ## __init__
    #  Initializes internal state of the Property object.
    #  @param _xml The Properties.xml path
    def __init__(self, _xml):
        doc = et.parse(pathByPlatform(_xml))
        self.__loadModule(doc.getroot())
    #ed
    
    ## __loadModule
    #  Loads the property value for the tag.
    #  @param _root The root node of the Properties.xml
    def __loadModule(self, _root):
        module_tag = _root.find('module')
        
        self.__loadLog(module_tag)
        self.__loadDatabase(module_tag)
        self.__loadGenerator(module_tag)
        self.module = Property.Module(module_tag.attrib['name'])
    #ed
    
    ## __loadLog
    #  Loads the property value for the tag.
    #  @param _module The module node of the Properties.xml
    def __loadLog(self, _module):
        log_tag = _module.find('log')
       
        self.log = Property.Log(log_tag.attrib['path'], log_tag.attrib['level'])
    #ed
    
    ## __loadDatabase
    #  Loads the property value for the tag.
    #  @param _module The module node of the Properties.xml
    def __loadDatabase(self, _module):
        db_tag = _module.find('database')
        
        self.database = Property.Database(db_tag.attrib['host'], int(db_tag.attrib['port']), db_tag.attrib['name'], db_tag.attrib['id'], db_tag.attrib['pw'])
    #ed
    
    ## __loadGenerator
    #  Loads the property value for the tag.
    #  @param _module The module node of the Properties.xml
    def __loadGenerator(self, _module):
        db_tag = _module.find('generator')
        
        self.generator = Property.Generator(int(db_tag.attrib['initial_answer_seed']), int(db_tag.attrib['answer_count']), int(db_tag.attrib['question_count']))
    #ed
#ec