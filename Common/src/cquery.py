## @package Generator
#  ggenerator.py
#
#  Represents Query class 

## Query
#
#  Returns query string with parameters.
class Query:

    ## select
    #  Returns select query string.
    #  @param _table A table name to execute query.
    #  @param _distinct A boolean for distinct query statement.
    #  @param _columns A list of column names to see.
    #  @param _conditions A list of column names for condition statement.
    #  @param _combine A string for condition combine.
    #  @return A query string.
    @staticmethod
    def select(_table, _distinct = False, _columns = None, _conditions = None, _combine = 'AND'):
        query = 'SELECT '
        if _distinct == True:
            query += 'DISTINCT '
        #ei
        if _columns == None:
            query += '* '
        else:
            tmp = ''
            for column in _columns:
                if not len(tmp) == 0:
                    tmp += ', '
                #ei
                tmp += column
            #ef
            query += tmp + ' '
        #ei
        query += 'FROM '+ _table
        if not _conditions == None:
            query += ' WHERE '
            tmp = ''
            for condition in _conditions:
                if not len(tmp) == 0:
                    tmp += ' ' + _combine + ' '
                #ei
                tmp += condition
            #ef            
            query += tmp + ' '
        #ei        
        query += ';'
        
        return query
    #ed
        
    ## count
    #  Returns select count query string.
    #  @param _table A table name to execute query.
    #  @param _distinct A boolean for distinct query statement.
    #  @param _columns A list of column names to see.
    #  @param _conditions A list of column names for condition statement.
    #  @param _combine A string for condition combine.
    #  @return A query string.
    @staticmethod
    def  count(_table, _distinct = False, _columns = None, _conditions = None, _combine = 'AND'):
        query = 'SELECT '
        if _distinct == True:
            query += 'DISTINCT '
        #ei
        if _columns == None:
            query += 'Count(*) '
        else:
            tmp = ''
            for column in _columns:
                if not len(tmp) == 0:
                    tmp += ', '
                #ei
                tmp += 'Count(' + column + ')'
            #ef
            query += tmp + ' '
        #ei
        query += 'FROM '+ _table
        if not _conditions == None:
            query += ' WHERE '
            tmp = ''
            for condition in _conditions:
                if not len(tmp) == 0:
                    tmp += ' ' + _combine + ' '
                #ei
                tmp += condition
            #ef            
            query += tmp + ' '
        #ei        
        query += ';'
        
        return query        
    #ed
    
    ## insert
    #  Returns insert query string.
    #  @param _table A table name to execute query.
    #  @param _values A list of all columnes.
    #  @param _columns A list of column names to insert value.
    #  @return A query string.
    @staticmethod
    def insert(_table, _values, _columns = None):
        # Returns insert query string with params.
        #
        query = 'INSERT INTO ' + _table + ' '
        if not _columns == None:
            tmp = ''
            for column in _columns:
                if not len(tmp) == 0:
                    tmp += ', '
                #ei
                tmp += column
            #ef
            query += '(' + tmp + ')'
        #ei
        tmp = ''
        for value in _values:
            if not len(tmp) == 0:
                tmp += ', '
            #ei
            tmp += value
        #ef
        query += 'VALUES (' + tmp + ');'
        
        return query
    #ed
    
    ## update
    #  Returns insert query string.
    #  @param _table A table name to execute query.
    #  @param _columns A list of column names to update value.
    #  @param _conditions A list of column names for condition statement.
    #  @param _combine A string for condition combine.
    #  @return A query string.
    @staticmethod
    def update(_table, _columns, _conditions = None, _combine = 'AND'):
        query = 'UPDATE ' + _table
        tmp = ''
        for column in _columns:
            if not len(tmp) == 0:
                tmp += ', '
            #ei
            tmp += column
        #ef
        query += ' SET ' + tmp
        if not _conditions == None:
            tmp = ''
            for condition in _conditions:
                if not len(tmp) == 0:
                    tmp += ' ' + _combine + ' '
                #ei
                tmp += condition
            #ef
            query += ' WHERE ' + tmp
        #ei
        query += ';'

        return query
    #ed
    
    ## delete
    #  Returns delete query string.
    #  @param _table A table name to execute query.
    #  @param _conditions A list of column names for condition statement.
    #  @param _combine A string for condition combine.
    #  @return A query string.
    @staticmethod
    def delete(_table, _conditions = None, _combine = 'AND'):
        query = 'DELETE FROM ' + _table
        if not _conditions == None:
            tmp = ''
            for condition in _conditions:
                if not len(tmp) == 0:
                    tmp += ' ' + _combine + ' '
                #ei
                tmp += condition
            #ef
            query += ' WHERE ' + tmp
        #ei
        query += ';'
        
        return query
    #ed
    
    ## create
    #  Returns create query string.
    #  @param _table A table name to execute query.
    #  @param _columns A list of column names to update value.
    #  @param _opt A string for table creation.
    #  @return A query string.
    @staticmethod
    def create(_table, _columns, _opt = None):
        query = 'CREATE TABLE ' + _table + '('
        tmp = ''
        for column in _columns:
            if not len(tmp) == 0:
                tmp += ', '
            #ei
            tmp += column
        #ef
        query += tmp + ')'
        if not _opt == None:
            query += ' ' + _opt
        #ei
        query += ';'
        
        return query
    #ed
    
    ## drop
    #  Returns drop query string.
    #  @param _table A table name to execute query.
    #  @return A query string.
    @staticmethod
    def drop(_table):
        query = 'DROP TABLE ' + _table + ';'
        
        return query
    #ed
#ec