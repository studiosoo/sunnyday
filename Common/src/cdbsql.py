## @package Common
#  cdbsql.py
#
#  Represents DBsql class.
import MySQLdb

## DBsql
#
#  Provides access to database.
#  TODO: using log
class DBsql:
    ## @var __db
    #  A MySQLdb instance.
    
    ## __init__
    #  Initializes internal state of the DBsql object by parameters.]
    #  @param _host A ip address of the database.
    #  @param _port A port number of the database.
    #  @param _name A database name string.
    #  @param _id A account name string for database access.
    #  @param _pw A password string for database access.
    def __init__(self, _host, _port, _name, _id, _pw):
        self.__db = MySQLdb.Connect(host = _host, port = _port, user = _id, passwd = _pw, db = _name)
    #ed

    ## select
    #  Executes a query and returns the result.
    #  @param _query A sql query string.
    def select(self, _query):
        cursor = self.__db.cursor()
        
        cursor.execute(_query)
        return cursor.fetchall()
    #ed
    
    ## execute
    #  Executes a query and commits.
    #  @param _query A sql query string.
    def execute(self, _query):
        cursor = self.__db.cursor()
        try:
            cursor.execute(_query)
            self.__db.commit()
        except Exception as ex:
            print(ex)
            self.__db.rollback()
        #et
    #ed
    
    ## close
    #  Closes database connection.
    def close(self):
        self.__db.close()
    #ed    
#ec