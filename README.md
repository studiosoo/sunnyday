# README

This is a project for the Sudoku puzzle web service. 

With python source files in the 'Generator' and 'Common' You can make a lot of Sudoku puzzles.

The 'WordPress' is the puzzle page in the 'http://sunnyday.studio903.kr'

- - -

# Getting Started Generator on CentOS 7

## Prerequisites 

[CentOS 7.x Download](https://www.centos.org/download/)

The project uses Python2.7 and MariaDB on CentOS 7.

The repository is on bitbucket.org. So you need a basic knowledge of how to use git.

Also, you need to know the basic development skills for Python, MariaDB and Linux command.

## Installing

### 1. CentOS

It is recommended to install the minimal install version of CentOS.

### 2. Python

```
~]$ sudo yum install -y python
~]$ sudo yum install -y MySQL-python
```

### 3. MariaDB

```
~]$ sudo yum install -y mariadb-server
~]$ sudo systemctl start mariadb
~]$ sudo systemctl enable mariadb
~]$ sudo mysql_secure_installation
```

### 4. Git

```
~]$ sudo yum install -y git
~]$ git config --global user.name "User Name"
~]$ git config --global user.email "user@email.com"
~]$ ssh-keygen
```

Register the contents of the generated .pub file at 'https://bitbucket.org/account/user/[user name]/ssh-keys/'

## Preparing

### 1. Clone Repository

```
~]$ git clone got@bitbucket.org:studio903/sunnyday.git ./sunnyday
```

### 2. Create Database and Tables

```
~]$ mysql -u root -p
)]> create database SUNNYDAY;
)]> create user 'sunnyday'@'localhost' identified by '****';
)]> grant all privileges on SUNNYDAY.* to sunnyday@'%';
)]> flush privileges;
)]> source [user_sunnyday_path]/Database/SUNNYDAY.sql
```

## Running

### 1. Edit Property

From the file '[user_sunnyday_path]/Generator/conf/properties.xml', you can change the settings for the database connection information and the generator.
 
### 2. Execute script

Run the script located in the '[user_sunnyday_path]/Generator/bin/' path

## Doxygen

To see the pre-generated Doxygen documentation, [Visit Here](http://doxygen.studio903.kr).

Before generating, You may need to modify INPUT_PATH or OUTPUT_PATH in the Doxyfile.

```
~]$ sudo yum install -y doxygen
~]$ doxygen [user_sunnyday_path]/Doxygen/Doxyfile
```

- - -

# Adding Sudoku puzzle on WordPress

To see the Sudoku puzzle in your web page, Add code lines below. 

In this case, the 'wp-sunnyday' is the folder where the necessary files in.

```
<!-- DIV FOR PUZZLE -->
<div id="puzzle"></div>
<!-- DIV FOR RESULT -->
<div id="result" style="display: none;">
<h4><strong>Thank You For Playing.</strong></h4>
</div>
<!-- SCRIPTS SHOULD BE UNDER DIV ELEMENTS -->
<script type="text/javascript" src="/wp-sunnyday/page_puzzle.php" ></script>
<script type="text/javascript" src="/wp-sunnyday/page_puzzle.js" ></script>
```

- - -

# Contact

If you want to know more about the project, please visit the wiki page below.

https://bitbucket.org/studio903/sunnyday/wiki/Home

And if you are interested in talking to us about the project, please contact.

E-mail: kind.jamie@gmail.com

Twitter: https://twitter.com/@kind_jamie

- - -

# License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
