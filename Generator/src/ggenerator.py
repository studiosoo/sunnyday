## @package Generator
#  @file ggenerator.py
#
#  Represents Generator class 
import random
import copy

from clogs import *

from gdifficulty import *
from ganswer import *
from gquestion import *

## Generator
#
#  Creates answers and its questions.
class Generator:
    ## @var answerCount
    #  A number of answers to create.
    ## @var questionCount
    #  A number of questions per level, symmetry in one answer. 
    ## @var answers
    #  A list contains created answers.
    ## @var questions
    #  A list contains created questions.
    ## @var lastAnswerSeed
    #  A answer seed value of the last answer item at the answer list.
    ## @var __initialAnswerSeed
    #  the first hash value for random.

    ## VERSION
    # When the logic or order of generating answers and questions are changed,
    #   the VERSION need to be increased manually.
    # 1701  2017.12.08  answer-questions with difficulties
    # 1702  2017.12.20  generating with multi-process which has own random object
    # 1801  2018.02.01  fixed multi-answer bug
    # 1802  2018.03.02  fixed LU-RD Symmerty, type 2, bug
    #
    VERSION = 1802
    
    ## DESCS
    #  A statement of the version.
    DESCS = 'fixed LU-RD Symmerty, type 2, bug'
    
    ## __init__
    #  Initializes internal state of the Generate object by parameters.
    #  @param _answerCount A number of answers to create.
    #  @param _questionCount A number of questions per level, symmetry in one answer.
    #  @param _initialAnswerSeed the first hash value for random.
    def __init__(self, _answerCount, _questionCount, _initialAnswerSeed):
        self.answerCount = _answerCount
        self.questionCount = _questionCount
        self.__initialAnswerSeed = _initialAnswerSeed
    #ed

    ## run
    #  Creates answers and questions. 
    def run(self):
        # Creates Answers
        #
        self.answers = []
        answerSeed = self.__initialAnswerSeed
        for i in range(self.answerCount):
            answer = Answer(answerSeed)
            answerSeed = answer.run() + 1
            answer.printGrid()
            self.answers.append(answer)
        #ef
        self.lastAnswerSeed = answerSeed - 1
        
        # Creates Questions
        #
        self.questions = []
        for answer in self.answers:
            question = Question(answer, self.questionCount)
            self.questions.append(question)
            question.run()
        #ef
                
        for question in self.questions:
            question.join()
        #ef       
        
        for question in self.questions:
            question.printGrids()
        #ef        
    #ed        
#ec
