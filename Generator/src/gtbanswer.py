## @package Generator
#  @file gtbanswer.py
#
#  Represents TBAnswer class.
import copy

## TBAnswer
#
#  Represents TB_ANSWER table.
class TBAnswer:
    ## NAME
    #  Table name
    NAME = 'TB_ANSWER'
    
    ## Columns
    #  Represents columns of the TB_ANSWER table.
    class Columns:
        ## @var idx
        #  A number of the TB_ANSWER table index.
        ## @var seed
        #  A number for unique answer creating.
        ## @var version
        #  A unique number of the generator.
        ## @var reg_tm
        #  A registration time of the answer.
        ## @var answer_str[]
        #  A correct answer string.
    
        ## __init__
        #  Initializes internal state of the Columns object by parameters.
        #  @param _idx A number of the TB_ANSWER table index.
        #  @param _seed A number for unique answer creating.
        #  @param _version A unique number of the generator.
        #  @param _reg_tm A registration time of the answer.
        #  @param _answer_str A correct answer string.
        def __init__(self, _idx, _seed, _version, _reg_tm, _answer_str):
            self.idx = _idx
            self.seed = _seed
            self.version = _version
            self.reg_tm = _reg_tm
            self.answer_str = copy.deepcopy(_answer_str)
        #ed
    #ec
            
    ## querySelectAll
    #  @return A query string.
    @staticmethod
    def querySelectAll():
        return 'SELECT * FROM ' +  TBAnswer.NAME + ';'
    #ed
    
    ## querySelect
    #  @param _idx A number of the TB_ANSWER table index.
    #  @return A query string.
    @staticmethod
    def querySelect(_idx):
        return 'SELECT * FROM ' +  TBAnswer.NAME + ' WHERE idx=' + str(_idx) + ';'
    #ed

    ## querySelectWhere
    #  @param _seed A number for unique answer creating.
    #  @param _version A unique number of the generator.
    #  @return A query string.
    @staticmethod
    def querySelectWhere(_seed, _version):
        return 'SELECT * FROM ' +  TBAnswer.NAME + ' WHERE seed=' + str(_seed) + ' AND version=' + str(_version) + ';'
    #ed
    
    ## queryUpdate
    #  @param _idx A number of the TB_ANSWER table index.
    #  @param _answer_str A answer string to update.
    #  @return A query string.
    @staticmethod
    def queryUpdate(_idx, _answer_str):
        return 'UPDATE ' +  TBAnswer.NAME + ' SET reg_tm=now(), answer_str=\'' + _answer_str + '\' WHERE idx=' + str(_idx) + ';'
    #ed
    
    ## queryInsert
    #  @param _seed A number for unique answer creating.
    #  @param _version A unique number of the generator.
    #  @param _answer_str A answer string to insert.
    #  @return A query string.
    @staticmethod
    def queryInsert(_seed, _version, _answer_str):
        query = 'INSERT INTO ' +  TBAnswer.NAME + '(seed, version, reg_tm, answer_str) '
        query += 'VALUES (' + str(_seed) + ', ' + str(_version) + ', now(), \'' + _answer_str + '\');'
        return query
    #ed
    
    ## queryDelete
    #  @param _idx A number of the TB_ANSWER table index.
    #  @return A query string.
    @staticmethod
    def queryDelete(_idx):
        return 'DELETE FROM ' +  TBAnswer.NAME + ' WHERE idx=' + str(_idx) + ';'
    #ed
#ec
