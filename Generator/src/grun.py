## @package Generator
#  @file grun.py
#
#  Runs Generator
import getopt
import platform
import sys
if platform.system() == 'Linux':
    sys.path.insert(0, '../../Common/src')
else:
    sys.path.insert(0, '..\..\Common\src')
#ei

from cproperty import *
from clogs import *

from ggenerator import *
from gdbaccessor import *

from gsolver import *

## main
#  Runs Generator and insert result into database.
def main():
    # For Test
    answer = [
        [9, 1, 7, 4, 6, 5, 2, 3, 8],
        [3, 8, 2, 7, 1, 9, 4, 6, 5],
        [6, 4, 5, 8, 2, 3, 1, 7, 9],
        [1, 5, 9, 2, 8, 7, 6, 4, 3],
        [4, 7, 3, 5, 9, 6, 8, 1, 2],
        [2, 6, 8, 3, 4, 1, 5, 9, 7],
        [7, 2, 1, 9, 5, 4, 3, 8, 6],
        [8, 9, 6, 1, 3, 2, 7, 5, 4],
        [5, 3, 4, 6, 7, 8, 9, 2, 1]] 
    question = [
	    [9, 1, 0, 4, 6, 0, 2, 0, 8],
        [3, 0, 2, 7, 0, 9, 0, 0, 0],
        [0, 4, 0, 8, 0, 3, 0, 7, 0],
        [1, 5, 9, 0, 0, 0, 0, 0, 0],
        [4, 0, 0, 0, 9, 0, 0, 0, 0],
        [0, 6, 8, 0, 0, 0, 0, 9, 0],
        [7, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 6, 0, 0, 2, 0, 0, 4],
        [5, 0, 0, 0, 0, 0, 0, 2, 1]]
    #Solver(question, answer)
    #return

    # Initializes property
    #    
    property = Property('../conf/properties.xml')
    
    # Initializes Logs
    #
    logInit(property.module.get('name'), property.log.get('path'))
    
    logINF('Generator::main start generating')    
    # Generates answers and questions
    #
    initialAnswerSeed = property.generator.get('initial_answer_seed')
    answerCount = property.generator.get('answer_count')
    questionCount = property.generator.get('question_count')
    logINF('Generator::main generator answer[' + str(answerCount) + '] question[' + str(questionCount) +']')
    
    # Database information
    #
    host = property.database.get('host')
    port = property.database.get('port')
    name  = property.database.get('name')
    id  = property.database.get('id')
    pw = property.database.get('pw')
    logINF('Generator::main database host[' + host + '] port[' + str(port) +'] name[' + name + ']')
    accessor = DBAccessor(host, port, name, id, pw)
    accessor.insertVersion(Generator.VERSION, Generator.DESCS)
    accessor.close()

    generated = 0
    while generated < answerCount:
        count = 3
        if answerCount - generated < count:
            count = answerCount - generated
        #ei

        logINF('Generator::main generator run count[' + str(count) + ']')
        generator = Generator(count, questionCount, initialAnswerSeed)        
        generator.run()        
        initialAnswerSeed = generator.lastAnswerSeed + 1
    
        logINF('Generator::main accessor insert')        
        accessor = DBAccessor(host, port, name, id, pw)        
        for answer in generator.answers:
            accessor.insertAnswer(answer.seed, Generator.VERSION, grid2str(answer.grid))
        #ef
    
        for question in generator.questions:
            for level in range(len(Difficulty.LEVELS)):
                for symmetry in range(len(Question.SYMMETRIES)):
                    for number in range(question.count):
                        answerSeed = question.answer.seed
                        seed = question.seeds[level][symmetry][number]
                        grid = grid2str(question.grids[level][symmetry][number])
                        accessor.insertQuestion(answerSeed, level, symmetry, seed, Generator.VERSION, grid)
                    #ef
                #ef
            #ef
        #ef        
                
        accessor.close()
        generated += count
        logINF('Generator::main generated [' + str(generated) + ' / ' + str(answerCount) + ']')
    #ef
       
    logINF('Generator::main done')
#ed

if __name__ == "__main__":
    main()
#ei
