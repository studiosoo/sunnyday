## @package Generator
#  @file gtbversion.py
#
#  Represents TBVersion class.
import copy

## TBVersion
#
#  Represents TB_VERSION table.
class TBVersion:
    ## NAME
    #  Table name
    NAME = 'TB_VERSION'
    
    ## Columns
    #  Represents columns of the TB_VERSION table.
    class Columns:
        ## @var version
        #  A unique number of the generator.
        ## @var reg_tm
        #  A registration time of the version.
        ## @var descs
        #  A statement of the version.
    
        ## __init__
        #  Initializes internal state of the Columns object by parameters.
        #  @param _version A unique number of the generator.
        #  @param _reg_tm A registration time of the version.
        #  @param _descs A statement of the version.
        def __init__(self, _version, _reg_tm, _descs):
            self.version = _version
            self.reg_tm = _reg_tm
            self.descs = _descs
        #ed
    #ec
           
    ## querySelectAll
    #  @return A query string.             
    @staticmethod
    def querySelectAll():
        return 'SELECT * FROM ' +  TBVersion.NAME + ';'
    #ed
    
    ## querySelect
    #  @param _version A unique number of the generator.
    #  @return A query string.    
    @staticmethod
    def querySelect(_version):
        return 'SELECT * FROM ' +  TBVersion.NAME + ' WHERE version=' + str(_version) + ';'
    #ed
    
    ## queryUpdate
    #  @param _version A unique number of the generator.
    #  @param _descs A statement of the version.
    #  @return A query string.
    @staticmethod
    def queryUpdate(_version, _descs):
        return 'UPDATE ' +  TBVersion.NAME + ' reg_tm=now(), SET descs=\'' + _descs + '\' WHERE version=' + str(_version) + ';'
    #ed
    
    ## queryInsert
    #  @param _version A unique number of the generator.
    #  @param _descs A statement of the version.
    #  @return A query string.
    @staticmethod
    def queryInsert(_version, _descs):
        query = 'INSERT INTO ' +  TBVersion.NAME + ' VALUES (' + str(_version) + ', now(), \'' + _descs + '\') ' 
        query += 'ON DUPLICATE KEY UPDATE reg_tm=now(), descs=\'' + _descs + '\';'
        return query
    #ed
    
    ## queryDelete
    #  @param _version A unique number of the generator.
    #  @return A query string.
    @staticmethod
    def queryDelete(_version):
        return 'DELETE FROM ' +  TBVersion.NAME + ' WHERE version=' + str(_version) + ';'
    #ed
#ec
