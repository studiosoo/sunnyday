## @package Generator
#  @file gdifficulty.py
#
#  Represents Difficulty class 

## Difficulty
#
#  Contains variables about difficulties.
#  TODO: load from database.
class Difficulty:
    ## @var name
    #  A name of the difficulty.
    ## @var blank
    #  A number of blanks in this difficulty.
    ## @var blankRange
    #  A range of extra-blanks in this difficulty.
    ## @var min
    #  A minimum number of one number to display.
    ## @var max
    #  A maximum number of one number to display.
    
    ## LEVELS
    #  The list contains level numbers.
    LEVELS = [0, 1, 2, 3, 4]
    
    ## NAMES
    #  The list contains names.
    NAMES = ['warmup', 'enjoyable', 'usually', 'challenging', 'madness']
    
    ## BLANKS
    #  The list contains a number of blanks.
    BLANKS = [26, 39, 49, 49, 50]
    
    ## BLANKRANGES
    #  The list contains a range of extra-blanks.
    BLANKRANGES = [1, 2, 2, 4, 6]  
    
    ## MINNUMS
    #  The list contains a minimum number of one number to display.
    MINNUMS = [4, 2, 2, 1, 0]
    
    ## MAXNUMS
    #  The list contains a maximum number of one number to display.
    MAXNUMS = [8, 7, 7, 6, 6]
        
    ## __init__
    #  Initializes internal state of the Difficulty object by _level.
    #  @param _level a level number.
    def __init__(self, _level):
        self.name = self.NAMES[_level]
        self.blank = self.BLANKS[_level]
        self.blankRange = self.BLANKRANGES[_level]
        self.min = self.MINNUMS[_level]
        self.max = self.MAXNUMS[_level]
    #ed
#ec
