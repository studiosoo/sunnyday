## @package Generator
#  @file gsolver.py
#
#  Represents Solver class
import sys
import copy

from clogs import *

## Solver
#
#  Solves the question to check usable the question.
class Solver:
    ## @var __questionGrid[][]
    #  A question grid to solve.
    ## @var __answerGrids[][][]
    #  A list of answer grids of the question.
    ## @var __correct
    #  True if there is only one answer and it is same as answer grid, False otherwise.
    
    ## __init__
    #  Initializes internal state of the Solver object by parameters.
    #  @param _questionGrid A question grid to solve.
    #  @param _answerGrid A answer grid of the question.
    def __init__(self, _questionGrid, _answerGrid):      
	#self.__print(_questionGrid)
	self.__questionGrid = copy.deepcopy(_questionGrid)
        self.__answerGrids = []
	self.__solve(self.__questionGrid, 0, 0, 0)

	#print(str(len(self.__answerGrids)))
	#if len(self.__answerGrids) == 2:
	#    self.__diff(self.__answerGrids[0], self.__answerGrids[1])
	#    self.__print(self.__answerGrids[0])
	#    self.__print(self.__answerGrids[1])
	#ei

	self.__correct = False
	if len(self.__answerGrids) == 1:
	    self.__correct = self.__compare(self.__answerGrids[0], _answerGrid)
	#ei
    #ed
    
    ## __compare
    #  Compares solved grid to answer grid.
    #  @param _solved A grid Solver solved.
    #  @param _answer A answer grid of the question.
    #  @return If successful, return True; otherwise return False.
    def __compare(self, _solved, _answer):
        for r in range(9):
            for c in range(9):
                if not _solved[r][c] == _answer[r][c]:
                    return False
                #ei
            #ef
        #ef
        
        return True
    #ef

    ## __diff
    #  If there is the same value on both sides at the same position, set it to 0, 
    #  leaving only the value of the position where the other value is. 
    #  @param _grid1 The grid to compare values of 9 x 9 
    #  @param _grid2 The grid to compare values of 9 x 9
    def __diff(self, _grid1, _grid2):
        for r in range(9):
            for c in range(9):
                if _grid1[r][c] == _grid2[r][c]:
                    _grid1[r][c] = 0
                    _grid2[r][c] = 0
                #ei
            #ef
        #ef
    #ed
    
    ## __solve
    #  Finds all answers of the question.
    #  @param _questionGrid A grid to solve.
    def __solve(self, _questionGrid, _r, _c, _depth):
        if 1 < len(self.__answerGrids):
            return
        #ei
        
        grid = copy.deepcopy(_questionGrid)
        cases = self.__cases(grid)        
        for ir in range(9 - _r):
            r = ir + _r
            for ic in range(9 - _c):
		c = ic + _c
                if len(cases[r][c]) == 0:
                    continue
                #ei
                for i in range(len(cases[r][c])):
                    n = cases[r][c][i]
        	    if not self.__selectable(grid, r, c, n):
                        continue
                    #ei
                                        
                    grid[r][c] = n
                    
                    if self.__isSolved(grid):
                        self.__answerGrids.append(grid)
		      	if 1 < len(self.__answerGrids):
		    	    return
        		#ei
                        continue
                    #ei
                
                    filled = self.__fill(copy.deepcopy(grid))
                    if self.__isSolved(filled):
                        self.__answerGrids.append(filled)
			if 1 < len(self.__answerGrids):
		            return
		        #ei
                        continue
                    #ei                    
                    
		    self.__solve(filled, r, c, _depth + 1)
 		    if 1 < len(self.__answerGrids):
            	        return
        	    #ei
                #ef        
            #ef
        #ef
    #ed
     
    ## __fill
    #  Fills insertable numbers in the grid.
    #  @param _questionGrid a grid to solve.
    #  @return If filling accured, return True; otherwise return False.
    def __fill(self, _questionGrid):
        answer = _questionGrid

	while True:
	    cases = self.__cases(answer)
	    end = True
	    for r in range(9):
                for c in range(9):
                    if len(cases[r][c]) == 1:
                        answer[r][c] = cases[r][c][0]
                        end = False
                    #ei
                #ef
            #ef
            if end:
                break
            #ei
        #ew

	return answer
    #ed
    
    ## __selectable
    #  Checks a number fillable at the location of parameters.
    #  @param _questionGrid A grid to solve.
    #  @param _r A position of the row.
    #  @param _c A position of the column.
    #  @parma _n A number to fill.
    #  @return If fillable, return True; otherwise return False.
    def __selectable(self, _questionGrid, _r, _c, _n):
        for i in range(9):
            if _questionGrid[_r][i] == _n or _questionGrid[i][_c] == _n:
                return False
            #ei
        #ef
        
        r = _r / 3 * 3
        c = _c / 3 * 3
        for br in range(r, r + 3):
            for bc in range(c, c + 3):
                if _questionGrid[br][bc] == _n:
                    return False
                #ei    
            #ef
        #ef
           
        return True
    #ed
    
    ## __cases
    #  Finds selectable numbers.
    #  @param _questionGrid A grid to solve.
    #  @return A grid contains the list of selectable numbers.
    def __cases(self, _questionGrid):
        cases = [[[] for r in range(9)] for c in range(9)]
        for r in range(9):
            for c in range(9):
                if not _questionGrid[r][c] == 0:
                    continue
                #ei 
                find = [0 for i in range(9)]
                for rr in range(9):
                    if not _questionGrid[rr][c] == 0:
                        find[_questionGrid[rr][c] - 1] = 1
                    #ei
                #ef
                for cc in range(9):
                    if not _questionGrid[r][cc] == 0:
                        find[_questionGrid[r][cc] - 1] = 1
                    #ei
                #ef
                br = r / 3 * 3
                bc = c / 3 * 3
                for bbr in range(br, br + 3):
                    for bbc in range(bc, bc + 3):
                        if not _questionGrid[bbr][bbc] == 0:
                            find[_questionGrid[bbr][bbc] - 1] = 1
                        #ei    
                    #ef
                #ef
                #print cases
                for i in range(9):
                    if find[i] == 0:
                        cases[r][c].append(i + 1)
                    #ei
                #ef
            #ef
        #ef
        
        return cases
    #ed
    
    ## __isSolved
    #  Checks whether found the new answer.
    #  @param _questionGrid A grid to solve.
    #  @return If successful, return True; otherwise return False.
    def  __isSolved(self, _questionGrid):
        for i in range(len(self.__answerGrids)):
            if self.__compare(self.__answerGrids[i], _questionGrid):
                return False
            #ei
        #ef

        for i in range(9):
            sums = [0, 0]
            for j in range(9):
                if _questionGrid[i][j] == 0:
                    return False
                #ei
                sums[0] = sums[0] + _questionGrid[i][j]
                sums[1] = sums[1] + _questionGrid[j][i]
            #ef
            if not sums[0] == 45 or not sums[1] == 45:
                return False
            #ei
        #ef
        
        for b in range(9):
            br = b / 3 * 3
            bc = b % 3 * 3
            sum = 0
            for r in range(br, br + 3):
                for c in range(bc, bc + 3):
                    sum = sum + _questionGrid[r][c]
                #ef
            #ef
            if not sum == 45:
                return False
            #ei
        #ef
        
        return True            
    #ed
    
    ## isUsable
    #  @return True if there is only one answer and it is same as answer grid, False otherwise.
    def isUsable(self):
        return (len(self.__answerGrids) == 1 and self.__correct)
    #ed    

    def __print(self, _grid):
        for r in range(9):
            row = ''
            for b in range(3):
                for c in range(b * 3, b * 3 + 3):
                    n = _grid[r][c]
                    if n == 0:
                        row = row + '[ ]'
                    else:
                        row = row + ' ' + str(n) + ' '
                    #ei
                #ef
                row = row + ' '
            #ef
            if r != 0 and r % 3 == 0:
                print('')
            #ei
            print(row)
        #ef

        print('--------')
    #ed

#ec
