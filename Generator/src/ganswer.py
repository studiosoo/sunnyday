## @package Generator
#  @file ganswer.py
#
#  Represents Answer class
import random
import copy

from clogs import *

from gdifficulty import *
from gsolver import *

## Answer
#
#  Creates a 9 by 9 answer grid by own seed value.
class Answer:
    ## @var __random
    #  A Random instance for unique creating.
    ## @var seed
    #  A number for unique answer creating.
    ## @var grid[row][column]
    #  A 9 by 9 dual integer list.
    
    ## __init__
    #  Initializes internal state of the Answer object by _initialSeed.
    #  @param _initialSeed the first hash value for random.
    def __init__(self, _initialSeed):                
        self.__initAnswer(_initialSeed)
    #ed

    ## run
    #  Creates answer grid. 
    #  If unsuccessful, it tries with the value incremented until it succeeds.
    #  @return The seed value that successfully created the answer.
    def run(self):
        while not self.__createAnswer():
            self.__initAnswer(self.seed + 1)
        #ew
        
        return self.seed
    #ed
        
    ## __initAnswer
    #  Initializes internal state of the Answer object with _seed.
    #  @param _seed the hash value for random
    def __initAnswer(self, _seed):       
        self.__random = random.Random()
        self.__random.seed(_seed)
        self.seed = _seed
        self.grid = [[0 for r in range(9)] for c in range(9)]
    #ed

    ## __createAnswer
    #  Creates a 9 by 9 answer grid.
    #  @return If successful, return True; otherwise return False.
    def __createAnswer(self):
        self.__fillFirstBlock()
        
        return self.__fillBlocks()
    #ed
    
    ## __fillFirstBlock
    #  Creates a 3 by 3 random-order grid at the left-up side of the answer grid.
    def __fillFirstBlock(self):
        block = self.__random.sample(range(1, 10), len(range(1, 10)))
        for i in range(9):
            self.grid[i/3][i%3] = block[i]
        #ef
    #ed
    
    ## __fillBlocks
    #  Fills remaining blocks
    #  @return If successful, return True; otherwise return False.
    def __fillBlocks(self):
        for b in range(1, 9):
            br = b / 3 * 3
            bc = b % 3 * 3
            if not self.grid[br][bc] == 0:
                continue
            #ei
    
            if self.__fillBlock(br, bc) == False:
                return False
            #ew           
        #ef
        
        return True
    #ed
    
    ## __fillBlock
    #  Creates a 3 by 3 random-order grid, fills block at the location (_br, _bc) of the grid.
    #  @param _br a block position of the row.
    #  @param _bc a block position of the column.
    #  @return If successful, return True; otherwise return False.
    def __fillBlock(self, _br, _bc):
        used = []
        for br in range(_br, _br + 3):
            for bc in range(_bc, _bc + 3):
                sn = range(1,10)
                for sr in range(br):
                    sn[self.grid[sr][bc] - 1] = 0
                #ef
                for sc in range(bc):
                    sn[self.grid[br][sc] - 1] = 0
                #ef
                for su in used:
                    sn[su - 1] = 0
                #ef
                sn = list(set(sn))
                sn.remove(0)
                if len(sn) == 0:
                    return False
                num = self.__random.choice(sn)
                self.grid[br][bc] = num
                used.append(num)
            #ef
        #ef
        
        return True
    #ed
    
    ## printGrid
    #  Writes logs with the seed and the grid.
    def printGrid(self):        
        logINF('Answer seed[' + str(self.seed) + ']' )
        
        for r in range(9):
            row = ''
            for b in range(3):
                for c in range(b * 3, b * 3 + 3):
                    n = self.grid[r][c]
                    if n == 0:
                        row = row + '[ ]'
                    else:
                        row = row + ' ' + str(n) + ' '
                    #ei                    
                #ef
                row = row + ' '
            #ef
            if r != 0 and r % 3 == 0:
                logINF('')
            #ei
            logINF(row)
        #ef
        
        logINF('--------')
    #ed 

    ## isCorrect
    #  Checks the _answer by the grid of this answer.
    #  @param _answer a 9 by 9 grid to check.
    #  @return If the _answer is correct, return True; otherwise return False.
    def isCorrect(self, _answer):
        for r in range(9):
            for c in range(9):
                if not self.grid[r][c] == _answer[r][c]:
                    return False
                #ei
            #ef
        #ef
        
        return True
    #ed
#ec
