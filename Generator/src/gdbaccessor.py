## @package Generator
#  @file gdbaccessor.py
#
#  Represents DBAccessor class 
import sys
import copy

from cproperty import * 
from clogs import *
from cdbsql import *

from gtbanswer import *
from gtbdifficulty import *
from gtbquestion import *
from gtbversion import *

## DBAccessor
#
#  Enables to access SUNNYDAY database using class methods.
class DBAccessor:
    ## @var __db
    #  DBSql instance for connecting database.
    
    ## __init__
    #  Initializes internal state of the DBAccessor object by _property.
    #  @param _host A ip address of the database.
    #  @param _port A port number of the database.
    #  @param _name A database name string.
    #  @param _id A account name string for database access.
    #  @param _pw A password string for database access.
    def __init__(self, _host, _port, _name, _id, _pw):
        self.__db = DBsql(_host, _port, _name, _id, _pw)
    #ed
    
    ## close
    #  Closes database connection.
    def close(self):
        self.__db.close()
    #ed
    
    ## insertVersion
    #  Executes insert query on the version table by parameters.
    #  @param _version A unique number of the generator.
    #  @param _descs A statement of the version.
    def insertVersion(self, _version, _descs):
        self.__db.execute(TBVersion.queryInsert(_version, _descs))
    #ed
     
    ## insertDifficulty
    #  Executes insert query on the difficulty table by parameters.
    #  @param _level A level number.
    #  @param _name A name of the difficulty.
    #  @param _blank_base_cnt A number of blanks in the difficulty.
    #  @param _blank_cnt_range A range of extra-blanks in the difficulty.
    #  @param _num_min_disp_cnt A minimum number of one number to display.
    #  @param _num_max_disp_cnt A maximum number of one number to display.
    def insertDifficulty(self, _level, _name, _blank_base_cnt, _blank_cnt_range, _num_min_disp_cnt, _num_max_disp_cnt):
        self.__db.execute(TBDifficulty.queryInsert(_level, _name, _blank_base_cnt, _blank_cnt_range, _num_min_disp_cnt, _num_max_disp_cnt))
    #ed
    
    ## insertAnswer
    #  Executes insert query on the answer table by parameters, 
    #  or execute update query when the row with the same seed and version is.
    #  @param _seed A number for unique answer creating.
    #  @param _version A unique number of the generator.
    #  @param _answer_str A correct answer string.
    def insertAnswer(self, _seed, _version, _answer_str):
        idx = 0

        rows = self.__db.select(TBAnswer.querySelectWhere(_seed, _version))        
        for row in rows:
            idx = row[0]
            break
        #ef

        if idx != 0:
            self.__db.execute(TBAnswer.queryUpdate(idx, _answer_str))
        else:
            self.__db.execute(TBAnswer.queryInsert(_seed, _version, _answer_str))
        #ei
    #ed
       
    ## insertQuestion
    #  Executes insert query on the question table by parameters,
    #  or execute update query when the row with the same answer_seed, seed and version is.
    #  @param _answer_seed A number for unique answer creating.
    #  @param _difficulty_level A level number of the question
    #  @param _symmetry A type number of symmetry.
    #  @param _seed A number for unique question creating.
    #  @param _version A unique number of the generator.
    #  @param _question_str A question string, 0 means blank.
    def insertQuestion(self, _answer_seed, _difficulty_level, _symmetry, _seed, _version, _question_str):
        idx = 0

        rows = self.__db.select(TBQuestion.querySelectWhere(_answer_seed, _difficulty_level, _symmetry, _seed, _version))        
        for row in rows:
            idx = row[0]
            break
        #ef

        if idx != 0:
            self.__db.execute(TBQuestion.queryUpdate(idx, _question_str))
        else:
            self.__db.execute(TBQuestion.queryInsert(_answer_seed, _difficulty_level, _symmetry, _seed, _version, _question_str))
        #ei
    #ed    
#ec
