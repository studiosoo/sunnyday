## @package Generator
#  @file gquestion.py
#
#  Represents Question class 
import random
import copy
from multiprocessing import Process, Pipe

from clogs import *

from ganswer import *
from gdifficulty import *
from gsolver import *

## Question
#
#  Creates a 9 by 9 question grids by level, symmetry and own seed value.
#  TODO: getting difficulty as parameter
class Question:
    ## @var __process
    #  A Process instance for multiprocessing.
    ## @var answer
    #  A Answer instance for questions.
    ## @var count
    #  A number of questions per level, symmetry in one answer.
    ## @var seeds[count]
    #  A list of numbers for unique question creating.
    ## @var grids[count][row][column]
    #  A list of the 9 by 9 dual integer list.
    
    ## SYMMETRIES
    #  A list of symmetries names.
    SYMMETRIES = ['LR', 'UD', 'LURD', 'LDRU']
    
    ## __init__
    #  Initializes internal state of the Question object by parameters.
    #  @param _answer A Answer instance for questions.
    #  @param _count A number of questions per level, symmetry in one answer.
    def __init__(self, _answer, _count):
        self.answer = _answer
        self.count = _count        
    #ed
        
    ## join
    #  Joins process for multiprocessing.
    def join(self):
        self.__process.join()        
    #ed
    
    ## run
    #  Creates question grids using multiprocessing.
    def run(self):
        p_conn, c_conn = Pipe()
        self.__process = Process(target=self.__run__, args=(self.answer.grid, self.count, c_conn,))
        self.__process.start()
        
        result = p_conn.recv()
        self.seeds = result[0]
        self.grids = result[1]
    #ed
        
    ## __run__
    #  Creates question grids by parameters and sends results via pipe.
    #  @param _answerGrid A answer grid for creating question.
    #  @param _count A number of questions per level, symmetry in one answer.
    #  @param _conn The child pipe connection for results return.
    #  @return sends the list contains a list of seeds and a list of grids.
    def __run__(self, _answerGrid, _count, _conn):
        rand = random.Random()        
        seeds = [[[0 for c in range(_count)] for s in range(len(Question.SYMMETRIES))] for l in range(len(Difficulty.LEVELS))]
        grids = [[[[[0 for r in range(9)] for c in range(9)] for c in range(_count)] for s in range(len(Question.SYMMETRIES))] for l in range(len(Difficulty.LEVELS))]
   
        for level in range(len(Difficulty.LEVELS)):
            for symmetry in range(len(Question.SYMMETRIES)):
                #For 1802 build
                if symmetry != 2:
                    continue
                for number in range(_count):
                    if number == 0:
                        seeds[level][symmetry][number] = 0
                    else:
                        seeds[level][symmetry][number] = seeds[level][symmetry][number - 1] + 1
                    #ei
                    rand.seed(seeds[level][symmetry][number])     		                               
                    while not self.__createQuestion(rand, _answerGrid, level, symmetry, number, grids):
                        seeds[level][symmetry][number] += 1
                        rand.seed(seeds[level][symmetry][number])
                    #ew            
                    logINF('generated question answer[' + str(self.answer.seed) + '] level[' + str(level) + '] symmetry[' + str(symmetry) + '] number[' + str(number) + '] seed[' + str(seeds[level][symmetry][number]) + ']')
                #ef
            #ef
        #ef       
        
        result = []
        result.append(seeds)
        result.append(grids)
        _conn.send(result)        
        _conn.close()
    #ed
    
    ## __createQuestion
    #  Creates question grid by parameters and copy to _grids for result return.
    #  @param _rand A Random instance for unique creating.
    #  @param _answerGrid A answer grid for creating question.
    #  @param _level A level number of the question.
    #  @param _symmetry A symmetry type number of the question.
    #  @param _number A position index in the grids list.
    #  @param _grids A list of grid for result return.
    #  @return If successful, return True; otherwise return False.
    def __createQuestion(self, _rand, _answerGrid, _level, _symmetry, _number, _grids):
        difficulty = Difficulty(_level)
    
        grid = [[0 for c in range(9)] for r in range(9)]
        grid = copy.deepcopy(_answerGrid)
        count = difficulty.blank + _rand.choice(range(difficulty.blankRange))
        
        center = count / 9
        left =  (count - center) / 2
        center = count - left * 2
    
        cb = _rand.sample(range(9), center)
        for i in range(center):
            r = 0
            c = 0
            if _symmetry == 0:
                # 0:LR
                r = cb[i]
                c = 4
            elif _symmetry == 1:
                # 1:UD
                r = 4
                c = cb[i]
            elif _symmetry == 2:
                # 2:LURD
                r = cb[i]
                c = 8 - cb[i]   
            elif _symmetry == 3:
                # 3:LDRU                                
                r = cb[i]
                c = cb[i]             
            #ei            
            grid[r][c] = 0
        #ef
    
            
        # makes all symmetries
        #
        h = _rand.sample(range(36), left)
        for i in range(left):            
            r = 0
            c = 0
            sr = 0
            sc = 0
            if _symmetry == 0:
                # 0:LR
                r = h[i] / 4
                c = h[i] % 4
                sr = r
                sc = 8 - c
            elif _symmetry == 1:
                # 1:UD
                r = h[i] / 9
                c = h[i] % 9
                sr = 8 - r
                sc = c                
            elif _symmetry == 2:
                # 2:LURD
                rr = [0, 8, 15, 21, 26, 30, 33, 35]
                for ri in range(len(rr)):
                    rri = len(rr) - 1 - ri
                    if rr[rri] <= h[i]:
                        r = rri
                        c = h[i] - rr[r]
                        break                    
                    #ei
                #ef                
                sr = 8 - c
                sc = 8 - r
            elif _symmetry == 3:
                # 3:LDRU
                rr = [0, 1, 3, 6, 10, 15, 21, 28]
                for ri in range(len(rr)):
                    rri = len(rr) - 1 - ri
                    if rr[rri] <= h[i]:
                        r = rri + 1
                        c = h[i] - rr[rri]
                        break                    
                    #ei
                #ef 
                sr = c
                sc = r
            #ei
            grid[r][c] = 0
            grid[sr][sc] = 0
        #ef    
    
        if not self.__checkNumberRange(difficulty, grid):
            return False
        #ei
        
        if not self.__checkEmptyBlock(grid):
            return False
        #ei
        
	#self.__printLevelSymmetryNumber(self.answer.seed, _level, _symmetry, _number, 0, grid)
        if not self.__checkQuestionUsable(grid, _answerGrid):
            return False
        #ei
    
        _grids[_level][_symmetry][_number] = copy.deepcopy(grid)
    
        return True
    #ed

    ## __checkNumberRange
    #  Checks for minimum and maximum number of one number in the question grid.
    #  @param _difficulty A Difficulty instance of the question.
    #  @param _grid A created question need to be checked.
    #  @return If successful, return True; otherwise return False.
    def __checkNumberRange(self, _difficulty, _grid):
        ns = [0 for i in range(9)]
        for r in range(9):
            for c in range(9):
                n = _grid[r][c]
                if not n == 0:
                    ns[n - 1] = ns[n - 1] + 1
                #ei
            #ef
        #ef
        for i in range(9):
            if ns[i] < _difficulty.min or _difficulty.max < ns[i]:
                return False
            #ei
        #ef

        return True
    #ed
    
    ## __checkEmptyBlock
    #  Checks for empty block in the question grid.
    #  @param _grid A created question need to be checked.
    #  @return If successful, return True; otherwise return False.
    def __checkEmptyBlock(self, _grid):       
        for b in range(9):
            sum = 0
            for r in range(b / 3 * 3, b / 3 * 3 + 3):
                for c in range(b % 3 * 3, b % 3 * 3 + 3):
                    sum = sum + _grid[r][c]
                #ef
            #ef
            if sum == 0:
                return False
            #ei
        #ef
        
        return True
    #ed
    
    ## __checkQuestionUsable
    #  Checks for question via Solver.
    #  @param _grid A created question need to be checked.
    #  @param _answerGrid A answer grid for creating question.
    #  @return If successful, return True; otherwise return False.
    def __checkQuestionUsable(self, _grid, _answerGrid):
        return Solver(_grid, _answerGrid).isUsable()
    #ed   
    
    ## printGrid
    #  Prints all grids in the question.
    def printGrids(self):
        for level in range(len(Difficulty.LEVELS)):        
            for symmetry in range(len(Question.SYMMETRIES)):
                for number in range(self.count):
                    seed = self.seeds[level][symmetry][number]
                    grid = self.grids[level][symmetry][number]
                    self.__printLevelSymmetryNumber(self.answer.seed, level, symmetry, number, seed, grid)
                #ef
            #ef
        #ef
    #ed
    
    ## __printLevelSymmetryNumber
    #  Prints grid in the question.
    #  @param _answerGrid A answer grid for creating question.
    #  @param _level A level number of the question.
    #  @param _symmetry A symmetry type number of the question.
    #  @param _number A position index in the grids list.
    #  @param _seed A seed number used to create the question grid.
    #  @param _grid A 9 by 9 dual integer list.
    def __printLevelSymmetryNumber(self, _answerSeed, _level, _symmetry, _number, _seed, _grid):
        logStr = 'Question'
        logStr += ' answerSeed[' + str(_answerSeed) + ']'
        logStr += ' level[' + str(_level) + ']'
        logStr += ' symmetry[' + str(_symmetry) + ']'
        logStr += ' seed[' + str(_seed) + ']'        
        logStr += ' number[' + str(_number) + ']'
        print(logStr)
        
        for r in range(9):
            row = ''
            for b in range(3):
                for c in range(b * 3, b * 3 + 3):
                    n = _grid[r][c]
                    if n == 0:
                        row = row + '[ ]'
                    else:
                        row = row + ' ' + str(n) + ' '
                    #ei                    
                #ef
                row = row + ' '
            #ef
            if r != 0 and r % 3 == 0:
                print('')
            #ei
            print(row)
        #ef
        
        print('--------')
    #ed
#ec
