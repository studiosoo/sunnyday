## @package Generator
#  @file gtbquestion.py
#
#  Represents TBQuestion class.
import copy

## TBQuestion
#
#  Represents TB_QUESTION table.
class TBQuestion:
    ## NAME
    #  Table name
    NAME = 'TB_QUESTION'
    
    ## Columns
    #  Represents columns of the TB_QUESTION table.
    class Columns:
        ## @var idx
        #  A number of the TB_QUESTION table index.
        ## @var answer_seed
        #  A number for unique answer creating.
        ## @var difficulty_level
        #  A level number of the question
        ## @var symmetry
        #  A type number of symmetry.
        ## @var seed
        #  A number for unique question creating.
        ## @var version
        #  A unique number of the generator.
        ## @var reg_tm
        #  A registration time of the answer.
        ## @var question_str[]
        #  A question string, 0 means blank.
    
        ## __init__
        #  Initializes internal state of the Columns object by parameters.
        #  @param _idx A number of the TB_QUESTION table index.
        #  @param _answer_seed A number for unique answer creating.
        #  @param _difficulty_level A level number of the question
        #  @param _symmetry A type number of symmetry.
        #  @param _seed A number for unique question creating.
        #  @param _version A unique number of the generator.
        #  @param _reg_tm A registration time of the question.
        #  @param _question_str A question string, 0 means blank.
        def __init__(self, _idx, _answer_seed, _difficulty_level, _symmetry, _seed, _version, _reg_tm, _question_str):
            self.idx = _idx
            self.answer_seed = _answer_seed
            self.difficulty_level = _difficulty_level
            self.symmetry = _symmetry
            self.seed = _seed
            self.version = _version
            self.reg_tm = _reg_tm
            self.question_str = copy.deepcopy(_question_str)
        #ed
    #ec
           
    ## querySelectAll
    #  @return A query string.           
    @staticmethod
    def querySelectAll():
        return 'SELECT * FROM ' +  TBQuestion.NAME + ';'
    #ed

    ## querySelect
    #  @param _idx A number of the TB_QUESTION table index.
    #  @return A query string.
    @staticmethod
    def querySelect(_idx):
        return 'SELECT * FROM ' +  TBQuestion.NAME + ' WHERE idx=' + str(_idx) + ';'
    #ed

    ## querySelectWhere
    #  @param _answer_seed A number for unique answer creating.
    #  @param _difficulty_level A level number of the question
    #  @param _symmetry A type number of symmetry.
    #  @param _seed A number for unique question creating.
    #  @param _version A unique number of the generator.
    #  @return A query string.
    @staticmethod
    def querySelectWhere(_answer_seed, _difficulty_level, _symmetry, _seed, _version):
        query = 'SELECT * FROM ' +  TBQuestion.NAME
        query += ' WHERE answer_seed=' + str(_answer_seed) + ' AND difficulty_level=' + str(_difficulty_level)
        query += ' AND symmetry=' + str(_symmetry) + ' AND seed=' + str(_seed) + ' AND version=' + str(_version) + ';'
        return query
    #ed
    
    ## queryUpdate
    #  @param _idx A number of the TB_QUESTION table index.
    #  @param _question_str A question string, 0 means blank.
    #  @return A query string.
    @staticmethod
    def queryUpdate(_idx, _question_str):      
        return 'UPDATE ' +  TBQuestion.NAME + ' SET reg_tm=now(),  question_str=\'' + _question_str + '\' WHERE idx=' + str(_idx) + ';'
    #ed
    
    ## queryInsert
    #  @param _answer_seed A number for unique answer creating.
    #  @param _difficulty_level A level number of the question
    #  @param _symmetry A type number of symmetry.
    #  @param _seed A number for unique question creating.
    #  @param _version A unique number of the generator.
    #  @param _question_str A question string, 0 means blank.
    #  @return A query string.
    @staticmethod
    def queryInsert(_answer_seed, _difficulty_level, _symmetry, _seed, _version, _question_str):
        query = 'INSERT INTO ' +  TBQuestion.NAME + '(answer_seed, difficulty_level, symmetry, seed, version, reg_tm, question_str) '
        query += 'VALUES (' + str(_answer_seed) + ', ' + str(_difficulty_level) + ', ' + str(_symmetry) + ', ' + str(_seed) + ', ' + str(_version) + ', now(), \'' + _question_str + '\') ' 
        return query
    #ed
    
    ## queryDelete
    #  @param _idx A number of the TB_QUESTION table index.
    #  @return A query string.
    @staticmethod
    def queryDelete(_idx):
        return 'DELETE FROM ' +  TBQuestion.NAME + ' WHERE idx=' + str(_idx) + ';'
    #ed    
#ec
