## @package Generator
#  @file gtbdifficulty.py
#
#  Represents TBDifficulty class.
import copy

## TBDifficulty
#
# Represents TB_DIFFICULTY table.
class TBDifficulty:
    ## NAME
    #  Table name
    NAME = 'TB_DIFFICULTY'
    
    ## Columns
    #  Represents columns of the TB_DIFFICULTY table.
    class Columns:
        ## @var level
        #  A level number.
        ## @var name
        #  A name of the difficulty.
        ## @var blank_base_cnt
        #  A number of blanks in the difficulty.
        ## @var blank_cnt_range
        #  A range of extra-blanks in the difficulty.
        ## @var num_min_disp_cnt
        #  A minimum number of one number to display.
        ## @var num_max_disp_cnt
        #  A maximum number of one number to display.
    
        ## __init__
        #  Initializes internal state of the Columns object by parameters.
        #  @param _level A level number.
        #  @param _name A name of the difficulty.
        #  @param _blank_base_cnt A number of blanks in the difficulty.
        #  @param _blank_cnt_range A range of extra-blanks in the difficulty.
        #  @param _num_min_disp_cnt A minimum number of one number to display.
        #  @param _num_max_disp_cnt A maximum number of one number to display.
        def __init__(self, _level, _name, _blank_base_cnt, _blank_cnt_range, _num_min_disp_cnt, _num_max_disp_cnt):
            self.level = _level
            self.name = _name
            self.blank_base_cnt = _blank_base_cnt
            self.blank_cnt_range = _blank_cnt_range
            self.num_min_disp_cnt = _num_min_disp_cnt
            self.num_max_disp_cnt = _num_max_disp_cnt
        #ed        
    #ec
           
    ## querySelectAll
    #  @return A query string.           
    @staticmethod
    def querySelectAll():
        return 'SELECT * FROM ' +  TB_DIFFICULTY.NAME + ';'
    #ed
    
    ## querySelect
    #  @param _level A level number.
    #  @return A query string.
    @staticmethod
    def querySelect(_level):
        return 'SELECT * FROM ' +  TB_DIFFICULTY.NAME + ' WHERE level=' + str(_level) + ';'
    #ed
    
    ## queryUpdate
    #  @param _level A level number.
    #  @param _name A name of the difficulty.
    #  @param _blank_base_cnt A number of blanks in the difficulty.
    #  @param _blank_cnt_range A range of extra-blanks in the difficulty.
    #  @param _num_min_disp_cnt A minimum number of one number to display.
    #  @param _num_max_disp_cnt A maximum number of one number to display.
    #  @return A query string.
    @staticmethod
    def queryUpdate(_level, _name, _blank_base_cnt, _blank_cnt_range, _num_min_disp_cnt, _num_max_disp_cnt):
        query = 'UPDATE ' +  TB_DIFFICULTY.NAME + ' '
        query += 'SET name=\'' + _name + '\' '
        query += 'blank_base_cnt=' + str(_blank_base_cnt) + ' '
        query += 'blank_cnt_range=' + str(_blank_cnt_range) + ' '
        query += 'num_min_disp_cnt=' + str(_num_min_disp_cnt) + ' '
        query += 'num_max_disp_cnt=' + str(_num_max_disp_cnt) + ' '
        query += 'WHERE level=' + str(_level) + ';'
        return query
    #ed
    
    ## queryInsert
    #  @param _level A level number.
    #  @param _name A name of the difficulty.
    #  @param _blank_base_cnt A number of blanks in the difficulty.
    #  @param _blank_cnt_range A range of extra-blanks in the difficulty.
    #  @param _num_min_disp_cnt A minimum number of one number to display.
    #  @param _num_max_disp_cnt A maximum number of one number to display.
    #  @return A query string.
    @staticmethod
    def queryInsert(_level, _name, _blank_base_cnt, _blank_cnt_range, _num_min_disp_cnt, _num_max_disp_cnt):
        query = 'INSERT INTO ' +  TB_DIFFICULTY.NAME + ' '
        query += 'VALUES (' + str(_level) + ', \'' + name + '\', ' + str(_blank_base_cnt) + ', ' + str(_blank_cnt_range) + ', ' + str(_num_min_disp_cnt) + ', ' + str(_num_max_disp_cnt) + ') '
        query += 'ON DUPLICATE KEY UPDATE name=\'' + _name + '\', blank_base_cnt=' + str(_blank_base_cnt) + ', blank_cnt_range=' + str(_blank_cnt_range) + ', num_min_disp_cnt=' + str(_num_min_disp_cnt) + ', num_max_disp_cnt=' + str(_num_max_disp_cnt) + ');'
        return query
    #ed
    
    ## queryDelete
    #  @param _level A level number.
    #  @return A query string.
    @staticmethod
    def queryDelete(_level):
        return 'DELETE FROM ' +  TB_DIFFICULTY.NAME + ' WHERE level=' + str(_level) + ';'
    #ed 
#ec
